package com.example.adamkovar.dp.model.Event;

import java.io.Serializable;

public enum EventType implements Serializable {

    LECTURE(1,"Lecture"),
    SEMINAR(2,"Seminar"),
    EXAM(3,"Exam"),
    DEADLINE(4,"Deadline");

    private int id;
    private String name;

    EventType(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
