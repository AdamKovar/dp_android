package com.example.adamkovar.dp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.adamkovar.dp.adapter.ExamListAdapter;
import com.example.adamkovar.dp.dbHelper.ExamDBHelper;
import com.example.adamkovar.dp.model.Course.Course;
import com.example.adamkovar.dp.model.Course.Exam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ExamListActivity extends AppCompatActivity {

    private Course course;
    private List<Exam> exams;
    private ExamDBHelper dbHelper;
    private TextView courseName;
    private TextView courseTotalPoints;
    private TextView courseMinimumPoints;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView noDataImage = findViewById(R.id.empty_list_image);
        dbHelper = new ExamDBHelper(this);
        course = (Course) getIntent().getSerializableExtra("course");
        exams = new ArrayList<>();
        courseName = findViewById(R.id.exam_list_course_name);
        courseTotalPoints = findViewById(R.id.exam_list_title_totalPoint_value);
        courseMinimumPoints = findViewById(R.id.exam_list_title_minPoint_value);

        courseName.setText(course.getName());
        courseTotalPoints.setText(countTotalPoints(exams).toString());
        courseMinimumPoints.setText(countMinimumPoints(exams).toString());
        ListView examListView = findViewById(R.id.exam_list_view);
        List<Exam> savedExams = dbHelper.getExamsByCourseIdAndPeriod(course.getCode(), course.getPeriod());
        if (savedExams != null) {
            exams.addAll(savedExams);
        }


        ExamListAdapter adapter = new ExamListAdapter(this, exams, courseTotalPoints, courseMinimumPoints );
        if (exams.isEmpty()) {
        } else {
            examListView.setAdapter(adapter);
        }
        examListView.setEmptyView(noDataImage);
        examListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openEditActivity(exams.get(i));
            }
        });


        FloatingActionButton fab = findViewById(R.id.newEventBtn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEditActivity();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        exams.clear();
        ListView examListView = findViewById(R.id.exam_list_view);
        List<Exam> savedExams = dbHelper.getExamsByCourseIdAndPeriod(course.getCode(), course.getPeriod());
        if (savedExams != null) {
            exams.addAll(savedExams);
        }
        ExamListAdapter adapter = new ExamListAdapter(this, exams,courseTotalPoints, courseMinimumPoints);
        if (exams.isEmpty()) {
        } else {
            examListView.setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();
        courseTotalPoints.setText(countTotalPoints(exams).toString());
        courseMinimumPoints.setText(countMinimumPoints(exams).toString());
    }

    public void openEditActivity() {
        Context context = getApplicationContext();
        Intent intent = new Intent(context, EditExamActivity.class);
        intent.putExtra("course", this.course);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void openEditActivity(Exam exam) {
        Context context = getApplicationContext();
        Intent intent = new Intent(context, EditExamActivity.class);
        intent.putExtra("exam", exam);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private Double countTotalPoints(List<Exam> examList) {
        Double totalPoints = 0.0;
        for (Exam exam : examList) {
            totalPoints += exam.getEarnsPoints();
        }
        return totalPoints;
    }

    private Double countMinimumPoints(List<Exam> examList) {
        Double minimumPoints = 0.0;
        for (Exam exam : examList) {
            minimumPoints += exam.getMinPoints();
        }
        return minimumPoints;
    }

}
