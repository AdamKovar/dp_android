package com.example.adamkovar.dp.model;

public class EmailPreview {

    private Integer id;

    private String from;

    private String sentDate;

    private String sentTime;

    private String title;

    private String status;

    public EmailPreview() {
    }

    public EmailPreview(Integer id, String from, String sentDate, String sentTime, String title, String status) {
        this.id = id;
        this.from = from;
        this.sentDate = sentDate;
        this.sentTime = sentTime;
        this.title = title;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
