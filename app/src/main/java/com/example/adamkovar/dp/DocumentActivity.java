package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.adamkovar.dp.adapter.DocumentAdapter;
import com.example.adamkovar.dp.model.Document.Document;

import java.io.File;
import java.util.Arrays;

public class DocumentActivity extends AppCompatActivity {

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";

    private long downloadID;


    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Fetching the download id received with the broadcast
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadID == id) {
                Toast.makeText(DocumentActivity.this, "Download Completed", Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        ListView documentListView = (ListView) findViewById(R.id.documentListView);
        DocumentAdapter adapter = new DocumentAdapter(this, Arrays.asList(Document.values()));
        documentListView.setAdapter(adapter);

        documentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                beginDownload(position);


                // Display the selected item text on TextView

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(onDownloadComplete);
        } catch (IllegalArgumentException e) {

        }
    }
    private void beginDownload(int index) {
        File file = new File(getExternalFilesDir(null), "file.pdf");
        /*
        Create a DownloadManager.Request with all the information necessary to start the download
         */
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(createUrl(index)))
                .setTitle("File")// Title of the Download Notification
                .setDescription("Downloading")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                .setRequiresCharging(false)// Set if charging is required to begin the download
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadID = downloadManager.enqueue(request);// enqueue puts the download request in the queue.
    }


    public String createUrl(int index) {
        String documentType = Document.findById(index);
        if(documentType != null) {
            return BASE_URL.concat("study/pdf?login=xjurciova&docType=").concat(documentType);
        }
        return "";
    }
}




