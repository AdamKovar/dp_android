package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.adamkovar.dp.adapter.EventTypeAdapter;
import com.example.adamkovar.dp.model.Event.Event;
import com.example.adamkovar.dp.model.Event.EventType;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class EventDetailActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, AdapterView.OnItemSelectedListener {

    private EditText eventName;
    private EditText eventDescription;
    private TextView eventStartDate;
    private TextView eventEndDate;
    private Button eventSaveBtn;
    private ImageButton startDateBtn;
    private ImageButton endDateBtn;
    private Spinner spinner;
    private List<EventType> eventTypeList;

    boolean isStartDateClicked;

    int year, month, day, hour, minute;
    int finalYear, finalMonth, finalDay, finalHour, finalMinute;

    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        event = (Event) getIntent().getSerializableExtra("Event");

        if (event == null) {
            event = new Event();
        }

        eventName = findViewById(R.id.eventDetailEventName);
        eventDescription = findViewById(R.id.eventDetailDescription);
        spinner = (Spinner) findViewById(R.id.eventDetailEventTypeSpinner);
        eventStartDate = findViewById(R.id.eventDetailStartDate);
        eventEndDate = findViewById(R.id.eventDetailEndDate);
        eventSaveBtn = (Button) findViewById(R.id.eventDetailSaveBtn);
        startDateBtn = (ImageButton) findViewById(R.id.eventDetailStartDateBtn);
        endDateBtn = (ImageButton) findViewById(R.id.eventDetailEndDateBtn);
        isStartDateClicked = false;
        eventTypeList = Arrays.asList(EventType.values());

        setTextViews();
        if (Boolean.parseBoolean(event.getOrigin())) {
            disableFields();
        }


        EventTypeAdapter eventTypeAdapter = new EventTypeAdapter(this, eventTypeList);
        spinner.setAdapter(eventTypeAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EventType eventType = (EventType) parent.getItemAtPosition(position);
                event.setEventType(eventType.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        eventSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(EventDetailActivity.this);

// 2. Chain together various setter methods to set the dialog characteristics
                    builder.setMessage(R.string.create_event_in_default_calendar_message)
                            .setTitle(R.string.create_event_in_default_calendar_title);

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            createEvent();
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            saveEvent();
                        }
                    });

// 3. Get the <code><a href="/reference/android/app/AlertDialog.html">AlertDialog</a></code> from <code><a href="/reference/android/app/AlertDialog.Builder.html#create()">create()</a></code>
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    //createEvent();+
                } else {

                }
            }
        });

        startDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartDateClick();

            }
        });
        eventStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStartDateClick();
            }
        });

        endDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEndDateClick();
            }
        });

        eventEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEndDateClick();
            }
        });

    }

    public void onStartDateClick() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        isStartDateClicked = true;
        DatePickerDialog datePickerDialog = new DatePickerDialog(EventDetailActivity.this, R.style.DialogTheme, this, year, month, day);
        datePickerDialog.show();

    }

    public void onEndDateClick() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        isStartDateClicked = false;
        DatePickerDialog datePickerDialog = new DatePickerDialog(EventDetailActivity.this, R.style.DialogTheme, this, year, month, day);
        datePickerDialog.show();
    }

    private void setTextViews() {
        this.eventName.setText(makeText(event.getName()));
        this.eventDescription.setText(makeText(event.getTeacher()));
        //this.spinner.setSelection(getPositionOfType());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");//formating according to my need
        String startDate = event.getStart();
        String endDate = event.getEnd();
        this.eventStartDate.setText(makeText(startDate));
        this.eventEndDate.setText(makeText(endDate));
        if (event.getName() != null) {
            setTitle(event.getName());
        } else {
            setTitle("Nová udalosť");
        }
    }

    private void disableFields() {
        eventName.setEnabled(false);
        eventDescription.setEnabled(false);
        spinner.setEnabled(false);
        eventStartDate.setEnabled(false);
        eventEndDate.setEnabled(false);
        eventSaveBtn.setEnabled(false);
        startDateBtn.setEnabled(false);
        endDateBtn.setEnabled(false);
        isStartDateClicked = false;
        eventSaveBtn.setVisibility(View.GONE);
        }

    private boolean validateFields() {
        Boolean isValid = false;
        if (TextUtils.isEmpty(eventName.getText())) {
            eventName.setError(getResources().getString(R.string.required_field_message));
            isValid = false;

        } else if (TextUtils.isEmpty(eventName.getText())){
            eventName.setError(getResources().getString(R.string.required_field_message));
            isValid = false;
        }
        return isValid;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        finalYear = year;
        finalMonth = month;
        finalDay = dayOfMonth;

        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(EventDetailActivity.this, R.style.DialogTheme, EventDetailActivity.this, hour, minute, true);
        timePickerDialog.show();


    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        finalHour = hourOfDay;
        finalMinute = minute;

        Calendar finalCalendar = Calendar.getInstance();
        finalCalendar.set(finalYear, finalMonth, finalDay, finalHour, finalMinute);

        if (isStartDateClicked) {
            getEventStartDate().setText(String.format("%1$tA %1$tb %1$td %1$tY at %1$tI:%1$tM %1$Tp", finalCalendar));
        } else {
            getEventEndDate().setText(String.format("%1$tA %1$tb %1$td %1$tY at %1$tI:%1$tM %1$Tp", finalCalendar));
        }

        isStartDateClicked = false;
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    private int getPositionOfType(EventType eventType) {
        for (int i = 0; i < this.eventTypeList.size(); i++) {
            if (eventTypeList.get(i).getName().equals(eventType.getName())) {
                return i;
            }
        }
        return 0;
    }

    public TextView getEventStartDate() {
        return eventStartDate;
    }

    public TextView getEventEndDate() {
        return eventEndDate;
    }

    public void createEvent() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALENDAR)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_CALENDAR)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            ContentResolver cr = this.getContentResolver();
            ContentValues cv = new ContentValues();
            cv.put(CalendarContract.Events.TITLE, event.getName());
            cv.put(CalendarContract.Events.DESCRIPTION, "Desc");
            cv.put(CalendarContract.Events.EVENT_LOCATION, event.getRoom());
            cv.put(CalendarContract.Events.DTSTART, Calendar.getInstance().getTimeInMillis());
            cv.put(CalendarContract.Events.DTEND, Calendar.getInstance().getTimeInMillis() + 60 * 60 * 1000);
            cv.put(CalendarContract.Events.CALENDAR_ID, 1);
            cv.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, cv);

            saveEvent();

            Toast.makeText(this, "Event saved", Toast.LENGTH_LONG).show();
        }
    }

    private String makeText(String text) {
        if (text == null) {
            return "";
        } else {
            return text;
        }
    }

    public void saveEvent() {

    }
}
