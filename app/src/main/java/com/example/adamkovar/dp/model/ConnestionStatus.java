package com.example.adamkovar.dp.model;

public class ConnestionStatus {


    private Boolean connected;

    private String message;

    public ConnestionStatus(){}

    public ConnestionStatus(Boolean connected, String message) {
        this.connected = connected;
        this.message = message;
    }

    public Boolean isConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
