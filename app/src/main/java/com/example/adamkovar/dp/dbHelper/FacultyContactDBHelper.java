package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adamkovar.dp.model.Faculty.FacultyContact;

import java.util.ArrayList;
import java.util.List;

public class FacultyContactDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String FACULTY_CONTACT_TABLE_NAME = "facultyContact";
    public static final String FACULTY_CONTACT_COLUMN_ID = "id";
    public static final String FACULTY_CONTACT_COLUMN_FACULTY = "faculty";
    public static final String FACULTY_CONTACT_COLUMN_NAME = "name";
    public static final String FACULTY_CONTACT_COLUMN_ADDRESS = "address";
    public static final String FACULTY_CONTACT_COLUMN_PHONE = "phone";
    public static final String FACULTY_CONTACT_COLUMN_FAX = "fax";
    public static final String FACULTY_CONTACT_COLUMN_MAIL = "mail";
    public static final String FACULTY_CONTACT_COLUMN_NOTE = "note";

    public FacultyContactDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS facultyContact");
        onCreate(db);
    }

    public boolean insertFacultyContact(FacultyContact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformFacultyContactToDb(contact);
        db.insert(FACULTY_CONTACT_TABLE_NAME, null, contentValues);
        return true;
    }

    public FacultyContact getFacultyContactByFaculty(String faculty) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContact where faculty = '" + faculty + "'", null);
        if(res.getCount() <= 0){
            return null;
        }
        res.moveToFirst();
        if (res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_ID)) != null) {
            return getFacultyContactFromDB(res);
        }
        return null;
    }

    public boolean updateFacultyContact(FacultyContact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformFacultyContactToDb(contact);
        db.update(FACULTY_CONTACT_TABLE_NAME, contentValues, "id = ? ",
                new String[]{contact.getId()});
        return true;
    }

    public List<FacultyContact> getAllFacultyContacts() {
        List<FacultyContact> facultyContacts = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContact", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_ID)) != null) {
                FacultyContact contact = getFacultyContactFromDB(res);
                facultyContacts.add(contact);
            }
            res.moveToNext();
        }
        return facultyContacts;
    }


    private ContentValues transformFacultyContactToDb(FacultyContact contact) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FACULTY_CONTACT_COLUMN_FACULTY, contact.getFaculty());
        contentValues.put(FACULTY_CONTACT_COLUMN_NAME, contact.getName());
        contentValues.put(FACULTY_CONTACT_COLUMN_ADDRESS, contact.getAddress());
        contentValues.put(FACULTY_CONTACT_COLUMN_PHONE, contact.getPhone());
        contentValues.put(FACULTY_CONTACT_COLUMN_FAX, contact.getFax());
        contentValues.put(FACULTY_CONTACT_COLUMN_MAIL, contact.getMail());
        contentValues.put(FACULTY_CONTACT_COLUMN_NOTE, contact.getNote());
        return contentValues;
    }


    private FacultyContact getFacultyContactFromDB(Cursor res) {
        FacultyContact contact = new FacultyContact(res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_ID)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_FACULTY)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_NAME)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_ADDRESS)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_PHONE)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_FAX)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_MAIL)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_COLUMN_NOTE))
        );
        return contact;
    }
}
