package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.dbHelper.ExamDBHelper;
import com.example.adamkovar.dp.model.Course.Course;
import com.example.adamkovar.dp.model.Course.Exam;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class CourseListAdapter extends ArrayAdapter<Course> {

    private static final String examMarkPrefix = "EXAM_MARK_";


    Context context;

    private List<Course> courseList = new ArrayList<>();

    public CourseListAdapter(Context c, List<Course> courseList) {
        super(c, R.layout.index_course_row);
        this.context = c;
        this.courseList = courseList;
    }

    public CourseListAdapter(Context c) {
        super(c, R.layout.index_course_row);
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.index_course_row, parent, false);
        TextView courseNameText = view.findViewById(R.id.courseName);
        TextView courseTotalPoint = view.findViewById(R.id.coursePercent);
        TextView courseMinPoint = view.findViewById(R.id.courseMinPoints);
        TextView courseExamType = view.findViewById(R.id.courseExamType);
        TextView course_mark = view.findViewById(R.id.course_mark);
        ImageView courseStatusImage = view.findViewById(R.id.course_status_image);
        Course course = courseList.get(position);
        courseNameText.setText(course.getName());
        course_mark.setText(getMark(course.getExamMark()));
        courseTotalPoint.setText(countTotalEarnedPoints(course.getCode(), course.getPeriod()));
        courseMinPoint.setText(Integer.toString(course.getMinExamPoints()));
        courseExamType.setText(getStringByIdName(getContext(), course.getExamType()));
        if(!course_mark.getText().equals("-")) {
            courseTotalPoint.setVisibility(View.GONE);
            courseMinPoint.setText(context.getString(R.string.optains_credists_label).concat(" ").concat(course.getCredits()));
        }
        if(((String)course_mark.getText()).contains("A")
                || ((String)course_mark.getText()).contains("B")) {
            courseStatusImage.setImageResource(R.drawable.smile_positive);

        } else if (((String)course_mark.getText()).contains("C")
                || ((String)course_mark.getText()).contains("D")
                || ((String)course_mark.getText()).contains("E")) {
            courseStatusImage.setImageResource(R.drawable.smile_neutral);
        } else if (((String)course_mark.getText()).contains("FN")
                || ((String)course_mark.getText()).contains("FX")) {
            courseStatusImage.setImageResource(R.drawable.smile_negative);
        }

        return view;
    }


    @Override
    public int getCount() {
        return courseList.size();
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public String countTotalEarnedPoints(String courseNo, String period) {
        ExamDBHelper examDBHelper = new ExamDBHelper(getContext());
        List<Exam> exams = examDBHelper.getExamsByCourseIdAndPeriod(courseNo, period);
        Double totalPoints = 0.0;
        if (exams != null) {
            for (Exam exam : exams) {
                totalPoints += exam.getEarnsPoints();
            }
        }
        return totalPoints.toString();
    }

    public static String getStringByIdName(Context context, String idName) {
        Resources res = context.getResources();
        return res.getString(res.getIdentifier(idName, "string", context.getPackageName()));
    }

    private String getMark(String examMark) {
        String mark = examMark.replace("EXAM_MARK_", "");
        if (mark.equals("NONE")) {
            return "-";
        }
        return mark;
    }
}
