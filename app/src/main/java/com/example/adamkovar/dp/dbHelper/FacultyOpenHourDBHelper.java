package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adamkovar.dp.model.Faculty.FacultyOpenHour;

import java.util.ArrayList;
import java.util.List;

public class FacultyOpenHourDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String FACULTY_OPEN_HOUR_TABLE_NAME = "facultyOpenHour";
    public static final String FACULTY_OPEN_HOUR_COLUMN_ID = "id";
    public static final String FACULTY_OPEN_HOUR_COLUMN_FACULTY = "faculty";
    public static final String FACULTY_OPEN_HOUR_COLUMN_DAY = "day";
    public static final String FACULTY_OPEN_HOUR_COLUMN_FROM = "openFrom";
    public static final String FACULTY_OPEN_HOUR_COLUMN_TO = "openTo";
    public static final String FACULTY_OPEN_HOUR_COLUMN_NOTE = "note";

    public FacultyOpenHourDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS facultyOpenHour");
        onCreate(db);
    }

    public boolean insertFacultyOpenHour(FacultyOpenHour facultyOpenHour) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformFacultyOpenHourToDb(facultyOpenHour);
        db.insert(FACULTY_OPEN_HOUR_TABLE_NAME, null, contentValues);
        return true;
    }

    public List<FacultyOpenHour> getAllFacultyOpenHourListByFaculty(String faculty) {
        List<FacultyOpenHour> facultyOpenHours = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyOpenHour where faculty = '" + faculty + "'", null);
        res.moveToFirst();
        if (res.getCount() <= 0) {
            return null;
        }
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_ID)) != null) {
                FacultyOpenHour openHour = getFacultyOpenHourFromDB(res);
                facultyOpenHours.add(openHour);
            }
            res.moveToNext();
        }
        return facultyOpenHours;
    }

    public FacultyOpenHour getAllFacultyOpenHourListByFacultyAndDayOfWeek(String faculty, String dayOfWeek) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyOpenHour where faculty = '" +
                faculty + "' and day = '" + dayOfWeek + "'", null);
        res.moveToFirst();
        if (res.getCount() <= 0) {
            return null;
        }
        if (res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_ID)) != null) {
            return getFacultyOpenHourFromDB(res);
        }
        return null;
    }


    public boolean updateFacultyOpenHour(FacultyOpenHour facultyOpenHour) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformFacultyOpenHourToDb(facultyOpenHour);
        db.update(FACULTY_OPEN_HOUR_TABLE_NAME, contentValues, "id = ? ",
                new String[]{facultyOpenHour.getId()});
        return true;
    }

    public List<FacultyOpenHour> getAllFacultyOpenHourList() {
        List<FacultyOpenHour> facultyOpenHours = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContactPerson", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_ID)) != null) {
                FacultyOpenHour openHour = getFacultyOpenHourFromDB(res);
                facultyOpenHours.add(openHour);
            }
            res.moveToNext();
        }
        return facultyOpenHours;
    }

    private ContentValues transformFacultyOpenHourToDb(FacultyOpenHour facultyOpenHour) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FACULTY_OPEN_HOUR_COLUMN_FACULTY, facultyOpenHour.getFaculty());
        contentValues.put(FACULTY_OPEN_HOUR_COLUMN_DAY, facultyOpenHour.getDayOfWeek());
        contentValues.put(FACULTY_OPEN_HOUR_COLUMN_FROM, facultyOpenHour.getFrom());
        contentValues.put(FACULTY_OPEN_HOUR_COLUMN_TO, facultyOpenHour.getTo());
        contentValues.put(FACULTY_OPEN_HOUR_COLUMN_NOTE, facultyOpenHour.getNote());
        return contentValues;
    }


    private FacultyOpenHour getFacultyOpenHourFromDB(Cursor res) {
        FacultyOpenHour facultyOpenHour = new FacultyOpenHour(res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_ID)),
                res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_FACULTY)),
                res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_DAY)),
                res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_FROM)),
                res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_TO)),
                res.getString(res.getColumnIndex(FACULTY_OPEN_HOUR_COLUMN_NOTE))
        );
        return facultyOpenHour;
    }
}
