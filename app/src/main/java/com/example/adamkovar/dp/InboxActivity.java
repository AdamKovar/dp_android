package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.adapter.MailListAdapter;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.EmailMessage;
import com.example.adamkovar.dp.model.EmailPreview;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InboxActivity extends AppCompatActivity {

    private RecyclerView emailListView;
    private List<EmailPreview> emailPreviewList = new ArrayList<>();
    private MailListAdapter mailListAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";
    private RequestQueue requestQueue;
    private int page = 0;
    private ProgressBar progressBarLoading;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        emailListView = findViewById(R.id.email_preview_list);
        layoutManager = new LinearLayoutManager(this);

        emailListView.setLayoutManager(layoutManager);
        progressBarLoading = findViewById(R.id.emailPrograsBar);
        //initialize adapter
        mailListAdapter = new MailListAdapter(this, emailPreviewList);

        //set adapter
        //hide loading bar
        progressBarLoading.setVisibility(View.VISIBLE);
        emailListView.setAdapter(mailListAdapter);
        emailListView.addItemDecoration(new DividerItemDecoration(emailListView.getContext(),
                DividerItemDecoration.VERTICAL));

        //add onscroll listener to the recycler view
        emailListView.addOnScrollListener(prOnScrollListener);


        requestQueue = Volley.newRequestQueue(this);

        //getdata from server
        getData();
    }

    private RecyclerView.OnScrollListener prOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (islastItemDisplaying(recyclerView)) {
                //so i would call the get data method here
                // show loading progress
                progressBarLoading.setVisibility(View.VISIBLE);
                getData();
                Log.i("ListActivity", "LoadMore");

            }
        }


    };

    private boolean islastItemDisplaying(RecyclerView recyclerView) {
        //check if the adapter item count is greater than 0
        if (recyclerView.getAdapter().getItemCount() != 0) {
            //get the last visible item on screen using the layoutmanager
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            //apply some logic here.
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }

        return false;
    }

    private void getData() {

        //add to requestQueue
        requestQueue.add(getDataFromServer(page));

        //increment page number
        page++;

        //remove any loading progress here
    }

    private JsonArrayRequest getDataFromServer(final int page) {
        //good practice to put a loading progress here
        SettingsDBHelper settingsDBHelper = new SettingsDBHelper(this);
        Setting setting = settingsDBHelper.getSettingByType(SettingType.LOGIN);

        //Json request begins
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(BASE_URL + "person/mail/" + String.valueOf(computeStartIndex(page)) +
                "/" + String.valueOf(computeEndIndex(page)) + "?login=" + setting.getValue() + "&inboxType=1",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //this is called when a response is gotten from the server


                        //after getting the data, I need to parse it the view
                        parseData(response);
                        Log.i("URL", BASE_URL + "person/mail/" + String.valueOf(page * 10) +
                                "/" + String.valueOf((page * 10) + 10) + "?login=" + setting.getValue() + "&inboxType=1");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //TODO
                    Toast.makeText(InboxActivity.this, "time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    //TODO
                } else if (error instanceof ServerError) {
                    //TODO
                    Toast.makeText(InboxActivity.this, "server error", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    //TODO
                    Toast.makeText(InboxActivity.this, "network error", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    //TODO
                    Toast.makeText(InboxActivity.this, "parse error", Toast.LENGTH_SHORT).show();
                }
                /* progressBar.setVisibility(View.GONE); */
                //If an error occurs that means end of the list has reached

                Toast.makeText(InboxActivity.this, "No More Result Available", Toast.LENGTH_SHORT).show();
            }
        });

        //some retrypoilicy for bad network
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //return array
        return jsonArrayRequest;
    }

    private void parseData(JSONArray response) {
        Log.i("Response: ", String.valueOf(response));
        //create a forLoop
        for (int i = 0; i < response.length(); i++) {
            EmailPreview preview = new EmailPreview();
            JSONObject jsonObject = null;
            //because from here they could be failures, so we use try and catch
            try {
                //get json object
                jsonObject = response.getJSONObject(i);
                Log.i("Response: ", String.valueOf(jsonObject));
                //add data from object to objects in ListUnit
                preview.setId(jsonObject.getInt("id"));
                preview.setFrom(jsonObject.getString("from"));
                preview.setSentDate(jsonObject.getString("sentDate"));
                preview.setSentTime(jsonObject.getString("sentTime"));
                preview.setTitle(jsonObject.getString("title"));
                preview.setStatus(jsonObject.getString("status"));

            } catch (JSONException e) {
                e.printStackTrace();

            }

            //add all the above to the array list
            emailPreviewList.add(preview);


        }

        //notify the adapter that some things has changed
        mailListAdapter.notifyDataSetChanged();

        progressBarLoading.setVisibility(View.INVISIBLE);
    }

    public static int computeStartIndex(int page) {
        if (page == 0) {
            return page + 1;
        } else {
            return (page * 10) + 1;
        }
    }

    public static int computeEndIndex(int page) {
        return (page + 1) * 10;
    }


}
