package com.example.adamkovar.dp;

import com.example.adamkovar.dp.adapter.CourseListAdapter;
import com.example.adamkovar.dp.dbHelper.CourseDBHelper;
import com.example.adamkovar.dp.model.Course.Course;
import com.example.adamkovar.dp.model.SemesterType;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class IndexActivity extends AppCompatActivity {

    /**
     * The {@link androidx.viewpager.widget.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * androidx.fragment.app.FragmentStatePagerAdapter.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static final String PREFS_NAME = "PrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        setTitle(R.string.title_activity_index);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int month = calendar.get(Calendar.MONTH);
        if(month <= 7 && month >=1) {
            TabLayout.Tab tab = tabLayout.getTabAt(1);
            tab.select();
        }


    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Integer position = getArguments().getInt(ARG_SECTION_NUMBER);
            String semesterType = SemesterType.getSemesterTypeById(position-1);
            List<Course> courses = new ArrayList<>();
            CourseDBHelper dbHelper = new CourseDBHelper(this.getContext());
            courses.addAll(dbHelper.getAllCoursesByAndPeriodAndSemesterType(getActualPeriod(), semesterType));
            View rootView;
            if(!courses.isEmpty()){
                rootView = inflater.inflate(R.layout.fragment_index, container, false);
                ListView courseListView = (ListView) rootView.findViewById(R.id.actual_subject_list_View);
                CourseListAdapter adapter = new CourseListAdapter(getActivity(),courses);
                courseListView.setAdapter(adapter);

                courseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // Get the selected item text from ListView
                        openCourseDetail(courses.get(position));
                        Log.d("Click on", Integer.toString(position));

                        // Display the selected item text on TextView

                    }
                });
            } else {
                rootView = inflater.inflate(R.layout.no_data_page, container, false);
            }

            return rootView;
        }

        public void openCourseDetail(Course course){
            Intent intent = new Intent(getActivity(), ExamListActivity.class);
            intent.putExtra("course", course);
            startActivity(intent);
        }

        private static String getActualPeriod(){
            String currentPeriod = "";
            Integer currentYear = Calendar.getInstance().get(Calendar.YEAR);
            Integer currentMonth = Calendar.getInstance().get(Calendar.MONTH);
            if(currentMonth >= 9) {
                currentPeriod = Integer.toString(currentYear).concat("/")
                        .concat(Integer.toString(currentYear+1));
            } else if(currentMonth <= 7) {
                currentPeriod = Integer.toString(currentYear-1).concat("/")
                        .concat(Integer.toString(currentYear));
            }
            return currentPeriod;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


}
