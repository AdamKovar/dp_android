package com.example.adamkovar.dp.model.Course;

public enum ExamTypeEnum {

    EXAM_TYPE_CREDIT(1),
    EXAM_TYPE_EXAM(2);

    private int id;

    ExamTypeEnum(int id){
        this.id = id;
    }

}
