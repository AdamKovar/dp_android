package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.adamkovar.dp.model.Course.Course;

import java.util.ArrayList;
import java.util.List;

public class CourseDBHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "DP.db";
    public static final String COURSE_TABLE_NAME = "course";
    public static final String COURSE_COLUMN_ID = "id";
    public static final String COURSE_COLUMN_NAME = "name";
    public static final String COURSE_COLUMN_REFERENCE = "ref";
    public static final String COURSE_COLUMN_CODE = "code";
    public static final String COURSE_COLUMN_TYPE = "type";
    public static final String COURSE_COLUMN_LANGUAGE = "lang";
    public static final String COURSE_COLUMN_EXAM_TYPE = "examType";
    public static final String COURSE_COLUMN_EXAM_ATTEMPT = "examAttempt";
    public static final String COURSE_COLUMN_EXAM_MARK = "examMark";
    public static final String COURSE_COLUMN_CREDITS = "credit";
    public static final String COURSE_COLUMN_SEMESTER_TYPE = "semesterType";
    public static final String COURSE_COLUMN_PERIOD = "period";
    public static final String COURSE_COLUMN_FACULTY = "faculty";
    public static final String COURSE_COLUMN_USERID = "userId";
    public static final String COURSE_COLUMN_MIN_CREDIT_POINTS = "minCredit";
    public static final String COURSE_COLUMN_MAX_CREDIT_POINTS = "maxCredit";
    public static final String COURSE_COLUMN_MIN_EXAM_POINTS = "minExam";
    public static final String COURSE_COLUMN_MAX_EXAM_POINTS = "maxExam";

    public CourseDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS course");
        onCreate(db);
    }

    public boolean insertCourse(Course course) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformCourseToDb(course);
        db.insert(COURSE_TABLE_NAME, null, contentValues);
        return true;
    }

    public Course getCourseByUserId(String userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from course where userId=" + userId + "",
                null);
        return getCourseFromDB(res);
    }

    public Course getCourseByUserIdAndCourseNumber(String userId, String courseNo) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from course where userId= '" + userId + "' " +
                " and code= '" + courseNo + "' ", null);
        if (res.getCount() <= 0) {
            return null;
        }
        res.moveToFirst();
        return getCourseFromDB(res);
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, COURSE_TABLE_NAME);
        return numRows;
    }

    public void getAllTableName() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                Log.d("table Name: ", c.getString(0));
                c.moveToNext();
            }
        }
    }

    public boolean updateCourse(Course course) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformCourseToDb(course);
        db.update(COURSE_TABLE_NAME, contentValues, "id = ? ",
                new String[]{course.getId()});
        return true;
    }

    public Integer deleteCourse(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(COURSE_TABLE_NAME,
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public List<Course> getAllCoursesByAndPeriodAndSemesterType(String period, String semesterType) {

        List<Course> courseList = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from course where period like '%" + period +
                        "%' and semesterType = '" + semesterType + "'",
                null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(COURSE_COLUMN_ID)) != null) {
                Course course = getCourseFromDB(res);
                courseList.add(course);
            }
            res.moveToNext();
        }
        return courseList;
    }

    public List<Course> getAllCourses() {

        List<Course> courseList = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from course", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(COURSE_COLUMN_ID)) != null) {
                Course course = getCourseFromDB(res);
                courseList.add(course);
            }
            res.moveToNext();
        }
        return courseList;
    }

    private Course getCourseFromDB(Cursor res) {
        Course course = new Course(res.getString(res.getColumnIndex(COURSE_COLUMN_ID)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_NAME)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_REFERENCE)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_CODE)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_TYPE)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_LANGUAGE)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_EXAM_TYPE)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_EXAM_ATTEMPT)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_EXAM_MARK)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_CREDITS)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_SEMESTER_TYPE)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_PERIOD)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_FACULTY)),
                res.getString(res.getColumnIndex(COURSE_COLUMN_USERID)),
                res.getInt(res.getColumnIndex(COURSE_COLUMN_MIN_CREDIT_POINTS)),
                res.getInt(res.getColumnIndex(COURSE_COLUMN_MAX_CREDIT_POINTS)),
                res.getInt(res.getColumnIndex(COURSE_COLUMN_MIN_EXAM_POINTS)),
                res.getInt(res.getColumnIndex(COURSE_COLUMN_MAX_EXAM_POINTS))

        );
        return course;
    }

    private ContentValues transformCourseToDb(Course course) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COURSE_COLUMN_NAME, course.getName());
        contentValues.put(COURSE_COLUMN_REFERENCE, course.getReference());
        contentValues.put(COURSE_COLUMN_CODE, course.getCode());
        contentValues.put(COURSE_COLUMN_TYPE, course.getType());
        contentValues.put(COURSE_COLUMN_LANGUAGE, course.getLanguage());
        contentValues.put(COURSE_COLUMN_EXAM_TYPE, course.getExamType());
        contentValues.put(COURSE_COLUMN_EXAM_ATTEMPT, course.getExamAttempt());
        contentValues.put(COURSE_COLUMN_EXAM_MARK, course.getExamMark());
        contentValues.put(COURSE_COLUMN_CREDITS, course.getCredits());
        contentValues.put(COURSE_COLUMN_SEMESTER_TYPE, course.getSemesterType());
        contentValues.put(COURSE_COLUMN_PERIOD, course.getPeriod());
        contentValues.put(COURSE_COLUMN_FACULTY, course.getFaculty());
        contentValues.put(COURSE_COLUMN_USERID, course.getLogin());
        contentValues.put(COURSE_COLUMN_MIN_CREDIT_POINTS, course.getMaxCreditPoints());
        contentValues.put(COURSE_COLUMN_MAX_CREDIT_POINTS, course.getMinCreditPoints());
        contentValues.put(COURSE_COLUMN_MIN_EXAM_POINTS, course.getMaxExamPoints());
        contentValues.put(COURSE_COLUMN_MAX_EXAM_POINTS, course.getMinExamPoints());
        return contentValues;
    }
}
