package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.model.Document.Document;

import java.util.ArrayList;
import java.util.List;

public class DocumentAdapter extends ArrayAdapter<Document> {

    Context context;

    private List<Document> documents = new ArrayList<>();

    public DocumentAdapter(Context c, List<Document> documents) {
        super(c, R.layout.download_document_row);
        this.context = c;
        this.documents = documents;
    }

    public DocumentAdapter(Context c) {
        super(c, R.layout.faculty_contact_item);
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.download_document_row, parent, false);

        TextView examName = view.findViewById(R.id.documentName);

        examName.setText(documents.get(position).name());
        return view;
    }


    @Override
    public int getCount() {
        return documents.size();
    }

    @NonNull
    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
