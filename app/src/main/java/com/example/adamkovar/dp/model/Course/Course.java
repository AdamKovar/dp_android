package com.example.adamkovar.dp.model.Course;

import java.io.Serializable;

public class Course implements Serializable {

    private String id;
    private String name;
    private String reference;
    private String code;
    private String type;
    private String language;
    private String examType;
    private String examAttempt;
    private String examMark;
    private String credits;
    private String semesterType;
    private String period;
    private String faculty;
    private String login;
    private Integer minCreditPoints;
    private Integer maxCreditPoints;
    private Integer minExamPoints;
    private Integer maxExamPoints;

    public Course(String id, String name, String reference, String code, String type,
                  String language, String examType, String examAttempt, String examMark,
                  String credits, String semesterType, String period, String faculty, String login,
                  Integer minCreditPoints, Integer maxCreditPoints, Integer minExamPoints,
                  Integer maxExamPoints) {
        this.id = id;
        this.name = name;
        this.reference = reference;
        this.code = code;
        this.type = type;
        this.language = language;
        this.examType = examType;
        this.examAttempt = examAttempt;
        this.examMark = examMark;
        this.credits = credits;
        this.semesterType = semesterType;
        this.period = period;
        this.faculty = faculty;
        this.login = login;
        this.minCreditPoints = minCreditPoints;
        this.maxCreditPoints = maxCreditPoints;
        this.minExamPoints = minExamPoints;
        this.maxExamPoints = maxExamPoints;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getExamAttempt() {
        return examAttempt;
    }

    public void setExamAttempt(String examAttempt) {
        this.examAttempt = examAttempt;
    }

    public String getExamMark() {
        return examMark;
    }

    public void setExamMark(String examMark) {
        this.examMark = examMark;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public String getSemesterType() {
        return semesterType;
    }

    public void setSemesterType(String semesterType) {
        this.semesterType = semesterType;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getMinCreditPoints() {
        return minCreditPoints;
    }

    public void setMinCreditPoints(Integer minCreditPoints) {
        this.minCreditPoints = minCreditPoints;
    }

    public Integer getMaxCreditPoints() {
        return maxCreditPoints;
    }

    public void setMaxCreditPoints(Integer maxCreditPoints) {
        this.maxCreditPoints = maxCreditPoints;
    }

    public Integer getMinExamPoints() {
        return minExamPoints;
    }

    public void setMinExamPoints(Integer minExamPoints) {
        this.minExamPoints = minExamPoints;
    }

    public Integer getMaxExamPoints() {
        return maxExamPoints;
    }

    public void setMaxExamPoints(Integer maxExamPoints) {
        this.maxExamPoints = maxExamPoints;
    }
}
