package com.example.adamkovar.dp.model.Document;

public enum Document {

    STUDY_CONFIRMATION(0),
    STUDY_CONFIRMATION_EN(1),
    REGISTRATION_SHEET(2),
    ENROLLMENT_SHEET(3);

    private int id;

    Document(int id) {
        this.id = id;
    }

    public static String findById(int id) {
        for(Document document : Document.values()) {
            if(document.id == id) {
                return document.name();
            }
        }
        return null;
    }
}
