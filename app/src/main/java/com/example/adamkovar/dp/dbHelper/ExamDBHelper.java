package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adamkovar.dp.model.Course.Course;
import com.example.adamkovar.dp.model.Course.Exam;
import com.example.adamkovar.dp.model.Person;

import java.util.ArrayList;
import java.util.List;

public class ExamDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String EXAM_TABLE_NAME = "exam";
    public static final String EXAM_COLUMN_ID = "id";
    public static final String EXAM_COLUMN_NAME = "name";
    public static final String EXAM_COLUMN_TYPE = "type";
    public static final String EXAM_COLUMN_COURSE_NO = "courseNo";
    public static final String EXAM_COLUMN_SEMESTER_TYPE = "semesterType";
    public static final String EXAM_COLUMN_PERIOD = "period";
    public static final String EXAM_COLUMN_FACULTY = "faculty";
    public static final String EXAM_COLUMN_USERID = "userId";
    public static final String EXAM_COLUMN_MAX_POINTS = "maxPoints";
    public static final String EXAM_COLUMN_MIN_POINTS = "minPoints";
    public static final String EXAM_COLUMN_EARNS_POINTS = "earnsPoints";

    public boolean insertExam(Exam exam) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformExamToDb(exam);
        db.insert(EXAM_TABLE_NAME, null, contentValues);
        return true;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS exam");
        onCreate(db);
    }

    public ExamDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public boolean updateExam(Exam exam) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformExamToDb(exam);
        db.update(EXAM_TABLE_NAME, contentValues, "id = ? ",
                new String[]{exam.getId()});
        return true;
    }

    public boolean deleteExam(Exam exam) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(EXAM_TABLE_NAME, "id = ? ",
                new String[]{exam.getId()});
        return true;
    }

    public List<Exam> getAllExams() {
        List<Exam> exams = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from exam", null);
        if(res.getCount() <= 0){
            return null;
        }
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(EXAM_COLUMN_ID)) != null) {
                Exam exam = getExamFromDB(res);
                exams.add(exam);
            }
            res.moveToNext();
        }
        return exams;
    }

    public List<Exam> getExamsByCourseIdAndPeriod(String courseNo, String period) {
        List<Exam> exams = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from exam where courseNo= '" + courseNo + "' " +
                "and period='" + period + "' ", null);
        if(res.getCount() <= 0){
            return null;
        }
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(EXAM_COLUMN_ID)) != null) {
                Exam exam = getExamFromDB(res);
                exams.add(exam);
            }
            res.moveToNext();
        }
        return exams;
    }

    private Exam getExamFromDB(Cursor res) {
        Exam exam = new Exam(res.getString(res.getColumnIndex(EXAM_COLUMN_ID)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_NAME)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_TYPE)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_COURSE_NO)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_SEMESTER_TYPE)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_PERIOD)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_FACULTY)),
                res.getString(res.getColumnIndex(EXAM_COLUMN_USERID)),
                res.getDouble(res.getColumnIndex(EXAM_COLUMN_MAX_POINTS)),
                res.getDouble(res.getColumnIndex(EXAM_COLUMN_MIN_POINTS)),
                res.getDouble(res.getColumnIndex(EXAM_COLUMN_EARNS_POINTS))
        );
        return exam;
    }

    private ContentValues transformExamToDb(Exam exam) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EXAM_COLUMN_NAME, exam.getName());
        contentValues.put(EXAM_COLUMN_TYPE, exam.getType());
        contentValues.put(EXAM_COLUMN_COURSE_NO, exam.getCourseNo());
        contentValues.put(EXAM_COLUMN_SEMESTER_TYPE, exam.getSemesterType());
        contentValues.put(EXAM_COLUMN_PERIOD, exam.getPeriod());
        contentValues.put(EXAM_COLUMN_FACULTY, exam.getFaculty());
        contentValues.put(EXAM_COLUMN_USERID, exam.getUserId());
        contentValues.put(EXAM_COLUMN_MAX_POINTS, exam.getMaxPoints());
        contentValues.put(EXAM_COLUMN_MIN_POINTS, exam.getMinPoints());
        contentValues.put(EXAM_COLUMN_EARNS_POINTS, exam.getEarnsPoints());
        return contentValues;
    }
}
