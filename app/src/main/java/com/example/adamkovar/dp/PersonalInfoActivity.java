package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.adamkovar.dp.adapter.PersonalInfoAdapter;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.PersonalInfo;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PersonalInfoActivity extends AppCompatActivity {

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);
        ListView personalInfoListViews = (ListView) findViewById(R.id.personal_info_list_view);
        SettingsDBHelper settingsDBHelper = new SettingsDBHelper(this);
        Setting setting = settingsDBHelper.getSettingByType(SettingType.LOGIN);
        List<PersonalInfo> personalInfoList  = new ArrayList<>();
        PersonalInfoAdapter adapter = new PersonalInfoAdapter(this, personalInfoList);
        personalInfoListViews.setAdapter(adapter);
        if(setting!= null) {
           getPersonalInfo(this, setting.getValue(), adapter);
        }

    }


    public static List<PersonalInfo> getPersonalInfo( Context context, String login, PersonalInfoAdapter adapter) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "person/allInfo?login=" + login;
        Log.d("url: ", url);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            List<PersonalInfo> personalInfoList = parsePersonalInfoJsonArray(response);
                            adapter.setPersonalInfoList(personalInfoList);
                            adapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Courses loading failed!",
                        Toast.LENGTH_SHORT).show();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        return new ArrayList<>();

    }

    public static List<PersonalInfo> parsePersonalInfoJsonArray(JSONArray jsonArray) {
        List<PersonalInfo> personalInfoList = new ArrayList<>();
        for (int i = 0; i <= jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                PersonalInfo info = new PersonalInfo(
                        object.getString("label"),
                        object.getString("value"));
                personalInfoList.add(info);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return personalInfoList;
    }
}
