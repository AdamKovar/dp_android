package com.example.adamkovar.dp;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adamkovar.dp.dbHelper.PersonDBHelper;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.ConnestionStatus;
import com.example.adamkovar.dp.model.Person;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;
import com.example.adamkovar.dp.service.ConnectionService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "PrefsFile";

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";

    private FirebaseAuth mAuth;
    private Button loginBtn;
    private EditText emailText;
    private EditText passwdText;

    PersonDBHelper dbHelper;

    private Person person;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSettingsTable(getApplicationContext());
        dbHelper = new PersonDBHelper(this);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        loginBtn = findViewById(R.id.login_button);
        emailText = findViewById(R.id.emailText);
        passwdText = findViewById(R.id.passwdText);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //TODO login for every time
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.apply();
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        Boolean isLogged = prefs.getBoolean("isLogged", false);
        if (isLogged) {
            changeActivity();
        }
    }

    public void logIn() {
        ConnestionStatus status = ConnectionService.isNetworkConnected(this);
        if (status.isConnected()) {
            if (TextUtils.isEmpty(emailText.getText())) {
                emailText.setError("Email is required!");
            } else if (TextUtils.isEmpty(passwdText.getText())) {
                passwdText.setError("Password is required!");
            }
            login(this, emailText.getText().toString(), passwdText.getText().toString());
        } else {
            Log.d("ERR", status.getMessage());
        }

    }

    public static void changeActivity(Context context, String login) {
        LoginActivity activity = (LoginActivity) context;
        SharedPreferences.Editor editor = activity.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean("isLogged", true);
        Person savedPerson = activity.dbHelper.getPersonByLogin(login);
        editor.putString("loggedAisId", savedPerson.getAisId());
        editor.putString("loggedPersonLogin", login);
        Log.d("saved person ais id: ", savedPerson.getAisId());
        editor.apply();
        Intent intent = new Intent(context, LoadingActivity.class);
        intent.putExtra("login", login);
        context.startActivity(intent);
    }

    public void changeActivity() {
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String login = preferences.getString("loggedPersonLogin", "");
        Person savedPerson = this.dbHelper.getPersonByLogin(login);
        Intent intent = new Intent(this, LoadingActivity.class);
        intent.putExtra("login", savedPerson.getLogin());
        startActivity(intent);
    }

    public static void getPersonalInfo(Context context, String login) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "person/info?login=" + login;
        Log.d("url: ", url);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Person person = parsePersonJsonObject(jsonObject);
                            PersonDBHelper dbHelper = new PersonDBHelper(context);
                            Person savedPerson = dbHelper.getPersonByAisId(person.getAisId());
                            if (savedPerson != null) {
                                person.setId(savedPerson.getId());
                                dbHelper.updatePerson(person);
                            } else {
                                dbHelper.insertPerson(person);
                            }
                            SettingsDBHelper settingsDBHelper = new SettingsDBHelper(context);
                            Setting setting = settingsDBHelper.getSettingByType(SettingType.LOGIN);
                            setting.setValue(person.getLogin());
                            settingsDBHelper.updateSetting(setting);
                            changeActivity(context, login);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Authentication failed.",
                        Toast.LENGTH_SHORT).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void login(Context context, String login, String password) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "login?login=" + login + "&password=" + password;
        Log.d("url: ", url);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        getPersonalInfo(context, login);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Authentication failed.",
                        Toast.LENGTH_SHORT).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private static Person parsePersonJsonObject(JSONObject object) throws JSONException {
        Person person = new Person(null,
                object.getString("name"),
                object.getString("aisId"),
                object.getString("login"),
                object.getString("title"),
                object.getString("dateOfBirth"),
                object.getString("gender"),
                object.getString("mail"));
        Log.d("Person", person.toString());
        return person;
    }

    public static void initSettingsTable(Context context) {
        SettingsDBHelper settingsDBHelper = new SettingsDBHelper(context);
        for(SettingType settingType : SettingType.values()) {
            Setting setting = settingsDBHelper.getSettingByType(settingType);
            if(setting == null) {
                Setting newSetting = new Setting();
                newSetting.setName(settingType.name());
                settingsDBHelper.insertSetting(newSetting);
            }
        }
    }


}
