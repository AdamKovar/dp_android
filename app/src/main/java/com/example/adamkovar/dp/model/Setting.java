package com.example.adamkovar.dp.model;

import java.io.Serializable;

public class Setting implements Serializable {

    private String id;

    private String name;

    private String value;

    public Setting() {
    }

    public Setting(String id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
