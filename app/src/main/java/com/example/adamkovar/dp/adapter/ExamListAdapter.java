package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.dbHelper.ExamDBHelper;
import com.example.adamkovar.dp.model.Course.Exam;
import com.example.adamkovar.dp.model.Event.Event;

import java.util.ArrayList;
import java.util.List;

public class ExamListAdapter extends ArrayAdapter<Exam> {

    Context context;

    private List<Exam> exams = new ArrayList<>();

    private TextView courseTotalPoints;
    private TextView courseMinimumPoints;

    public ExamListAdapter(Context c, List<Exam> exams, TextView courseTotalPoints, TextView courseMinimumPoints) {
        super(c, R.layout.faculty_contact_item);
        this.context = c;
        this.exams = exams;
        this.courseTotalPoints = courseTotalPoints;
        this.courseMinimumPoints = courseMinimumPoints;
        courseTotalPoints.setText(countTotalPoints(exams).toString());
        courseMinimumPoints.setText(countMinimumPoints(exams).toString());
    }

    public ExamListAdapter(Context c) {
        super(c, R.layout.faculty_contact_item);
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.exam_list_row, parent, false);
        if(!exams.isEmpty()) {
            TextView examName = view.findViewById(R.id.exam_row_name);
            TextView examMimPoints = view.findViewById(R.id.exam_row_min_points);
            TextView examEarnedPoints = view.findViewById(R.id.exam_row_earned_points);
            TextView examType = view.findViewById(R.id.exam_row_exam_type);
            ImageButton deleteExamBtn = view.findViewById(R.id.exam_row_delete_btn);
            deleteExamBtn.setTag(position);
            examName.setText(exams.get(position).getName());
            examMimPoints.setText(Double.toString(exams.get(position).getMinPoints()));
            examEarnedPoints.setText(Double.toString(exams.get(position).getEarnsPoints()));
            if("Exam".equals(exams.get(position).getType())) {
                examType.setText(context.getResources().getString(R.string.exam_edit_exam_type_exam_label));
            } else if("Credit".equals(exams.get(position).getType())) {
                examType.setText(context.getResources().getString(R.string.exam_edit_exam_type_credit_label));
            }



            deleteExamBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position=(Integer)view.getTag();
                    deleteExam(exams.get(position));
                    exams.remove(position);
                    courseTotalPoints.setText(countTotalPoints(exams).toString());
                    courseMinimumPoints.setText(countMinimumPoints(exams).toString());
                    notifyDataSetChanged();
                }
            });
        }
        return view;
    }


    @Override
    public int getCount() {
        return exams.size();
    }

    @NonNull
    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

    private void deleteExam(Exam exam) {
        ExamDBHelper dbHelper = new ExamDBHelper(getContext());
        dbHelper.deleteExam(exam);
    }

    private Double countTotalPoints(List<Exam> examList) {
        Double totalPoints = 0.0;
        for (Exam exam : examList) {
            totalPoints += exam.getEarnsPoints();
        }
        return totalPoints;
    }

    private Double countMinimumPoints(List<Exam> examList) {
        Double minimumPoints = 0.0;
        for (Exam exam : examList) {
            minimumPoints += exam.getMinPoints();
        }
        return minimumPoints;
    }
}
