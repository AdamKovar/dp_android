package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.model.Event.EventType;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class EventTypeAdapter extends ArrayAdapter<EventType> {

    public EventTypeAdapter(Context context, List<EventType> eventTypeArrayList) {
        super(context, 0, eventTypeArrayList);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.event_type_spinner_row, parent, false
            );
        }
        TextView textViewName = convertView.findViewById(R.id.eventTypeLabel);

        EventType eventType = getItem(position);

        if (eventType != null) {
            textViewName.setText(eventType.getName());
        }

        return convertView;
    }
}
