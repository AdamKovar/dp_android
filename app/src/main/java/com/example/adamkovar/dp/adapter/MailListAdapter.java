package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.adamkovar.dp.EmailDetailActivity;
import com.example.adamkovar.dp.InboxActivity;
import com.example.adamkovar.dp.MainActivity;
import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.EmailMessage;
import com.example.adamkovar.dp.model.EmailPreview;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MailListAdapter extends RecyclerView.Adapter<MailListAdapter.MyViewHolder> {

    private List<EmailPreview> emailPreviewList;
    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";
    private Context context;
    private EmailMessage selectedMail;
    private RequestQueue requestQueue;

    //create a class accessible from othe activities
    public MailListAdapter(Context context, List<EmailPreview> listUnitList) {
        this.context = context;
        this.emailPreviewList = listUnitList;

    }

    @Override
    public MailListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //this method specifies the layout resource to be used, which is list_unit.xml
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.email_preview_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MailListAdapter.MyViewHolder holder, int position) {
        //this method helps to get the position, and set parameters, more like feeling a spreadsheet of different columns and rows from a form filled by a client
        EmailPreview preview = emailPreviewList.get(position);
//fill up contents here
        holder.emailSubject.setText(preview.getTitle());
        holder.emailSender.setText(preview.getFrom());
        holder.emailDate.setText(preview.getSentDate());
        holder.emailTime.setText(preview.getSentTime());
        if(preview.getFrom().toCharArray()[0] == 'x') {
            holder.emailAcronym.setText(preview.getFrom().substring(1,2).toUpperCase());
        } else {
            holder.emailAcronym.setText(preview.getFrom().substring(0,1).toUpperCase());
        }
        requestQueue = Volley.newRequestQueue(this.context);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mail", String.valueOf(preview.getId()));
                requestQueue.add(showMail(preview.getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return emailPreviewList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView emailSubject, emailSender, emailTime, emailDate, emailAcronym;

        public MyViewHolder(View itemView) {
            super(itemView);

            //define views i want to fill up with something here
            emailSubject = itemView.findViewById(R.id.mail_row_subject);
            emailSender = itemView.findViewById(R.id.mail_row_sender);
            emailDate = itemView.findViewById(R.id.mail_row_date);
            emailTime = itemView.findViewById(R.id.mail_row_time);
            emailAcronym = itemView.findViewById(R.id.mail_acronym);
        }
    }

    public JsonObjectRequest showMail(int messageId) {
        return getEmailDataFromServer(messageId);
    }

    private void changeActivity() {
        Intent intent = new Intent(context, EmailDetailActivity.class);
        intent.putExtra("EmailMessage", this.selectedMail);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private JsonObjectRequest getEmailDataFromServer(final int messageId) {
        //good practice to put a loading progress here
        SettingsDBHelper settingsDBHelper = new SettingsDBHelper(this.context);
        Setting setting = settingsDBHelper.getSettingByType(SettingType.LOGIN);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                BASE_URL + "person/mail/" +
                        String.valueOf(messageId) + "?login=" + setting.getValue() + "&inboxType=1",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Do something with response
                        //mTextView.setText(response.toString());

                        // Process the JSON
                        parseEmailData(response);
                        Log.d("mail", "OK");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something when error occurred
                        Log.d("mail", "FAILED");
                    }
                }
        );
        return jsonObjectRequest;
    }

    private void parseEmailData(JSONObject jsonObject) {
        EmailMessage email = new EmailMessage();
        //create a forLoop
            //because from here they could be failures, so we use try and catch
            try {
                //get json object

                Log.i("Response: ", String.valueOf(jsonObject));
                //add data from object to objects in ListUnit
                email.setSubject(jsonObject.getString("subject"));
                email.setTo(jsonObject.getString("to"));
                email.setFrom(jsonObject.getString("from"));
                email.setSentDate(jsonObject.getString("sentDate"));
                email.setSentTime(jsonObject.getString("sentTime"));
                email.setCc(jsonObject.getString("cc"));
                email.setMessage(jsonObject.getString("message"));

            } catch (JSONException e) {
                e.printStackTrace();

            }

            //add all the above to the array list




        this.selectedMail = email;
        changeActivity();
        //notify the adapter that some things has changed
    }

}