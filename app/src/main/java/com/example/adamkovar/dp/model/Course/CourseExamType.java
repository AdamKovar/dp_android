package com.example.adamkovar.dp.model.Course;

public enum CourseExamType {

    COURSE_EXAM_TYPE_TEST(0, "Zápočet", ""),
    COURSE_EXAM_TYPE_EXAM(1, "Skúška", "");

    private int id;

    private String nameSK;

    private String nameEN;

    CourseExamType(int id, String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
    }

}
