package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.adamkovar.dp.dbHelper.ExamDBHelper;
import com.example.adamkovar.dp.model.Course.Course;
import com.example.adamkovar.dp.model.Course.Exam;

import java.util.List;

public class EditExamActivity extends AppCompatActivity {

    private Exam exam;
    private Course course;
    RadioGroup radioGroup;
    RadioButton examType;
    RadioButton creditExamType;
    RadioButton examExamType;
    ExamDBHelper dbHelper;
    EditText examName ;
    EditText examMinPoints;
    EditText examMaxPoints;
    EditText examEarnedPoints ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_exam);
        Exam examToEdit = (Exam) getIntent().getSerializableExtra("exam");
        this.course = (Course) getIntent().getSerializableExtra("course");
        if (examToEdit != null) {
            this.exam = examToEdit;
        } else {
            this.exam = new Exam();
        }

        dbHelper = new ExamDBHelper(this);

        examName = findViewById(R.id.exam_edit_name_value);
        examMinPoints = findViewById(R.id.exam_edit_min_points_value);
        examMaxPoints = findViewById(R.id.exam_edit_max_points_value);
        examEarnedPoints = findViewById(R.id.exam_edit_earned_points_value);
        radioGroup = findViewById(R.id.exam_edit_type_rg);
        creditExamType = findViewById(R.id.exam_edit_exam_type_Credit);
        examExamType = findViewById(R.id.exam_edit_exam_type_Exam);
        Button saveBtn = findViewById(R.id.exam_edit_save_btn);
         if(exam.getId() != null) {
             examName.setText(exam.getName());
             examMinPoints.setText(exam.getMinPoints().toString());
             examMaxPoints.setText(exam.getMaxPoints().toString());
             examEarnedPoints.setText(exam.getEarnsPoints().toString());
             if("Exam".equals(exam.getType())) {
                 examExamType.setChecked(true);
             } else if("Credit".equals(exam.getType())) {
                 creditExamType.setChecked(true);
             }
         } else {
             creditExamType.setChecked(true);
         }


        saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkRequiredField()) {
                    exam.setName(examName.getText().toString());
                    exam.setMinPoints(Double.parseDouble(examMinPoints.getText().toString()));
                    exam.setMaxPoints(Double.parseDouble(examMaxPoints.getText().toString()));
                    exam.setEarnsPoints(Double.parseDouble(examEarnedPoints.getText().toString()));
                    if(course != null) {
                        exam.setCourseNo(course.getCode());
                        exam.setPeriod(course.getPeriod());
                        exam.setFaculty(course.getFaculty());
                        exam.setSemesterType(course.getSemesterType());
                        exam.setUserId(course.getLogin());
                    }
                    if(examExamType.isChecked()) {
                        exam.setType("Exam");
                    } else if(creditExamType.isChecked()) {
                        exam.setType("Credit");
                    }
                    //DO SOMETHING! {RUN SOME FUNCTION ... DO CHECKS... ETC}
                    saveExam();
                }

            }
        });
    }

    public void saveExam(){
        if(exam.getId() == null) {
            dbHelper.insertExam(this.exam);
        } else {
            dbHelper.updateExam(this.exam);
        }
        List<Exam> savedExam = dbHelper.getAllExams();
        changeActivity();
    }

    public void changeActivity(){
        Context context = getApplicationContext();
        Intent intent = new Intent(context, ExamListActivity.class);
        intent.putExtra("course", this.course);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        finishActivity(0);
    }

    public void examTypeClick(View view){
        int radioBtnId = radioGroup.getCheckedRadioButtonId();
        examType = findViewById(radioBtnId);
    }

    private boolean checkRequiredField() {
        Boolean validation = true;
        if(TextUtils.isEmpty(examName.getText())) {
            examName.setError(getResources().getString(R.string.required_field_message));
            validation = false;
        }
        if(TextUtils.isEmpty(examMinPoints.getText()) ) {
            examMinPoints.setError(getResources().getString(R.string.required_field_message));
            validation = false;
        }
        if(TextUtils.isEmpty(examMaxPoints.getText())) {
            examMaxPoints.setError(getResources().getString(R.string.required_field_message));
            validation = false;
        }
        if(TextUtils.isEmpty(examEarnedPoints.getText())) {
            examEarnedPoints.setError(getResources().getString(R.string.required_field_message));
            validation = false;
        }
        return validation;
    }
}
