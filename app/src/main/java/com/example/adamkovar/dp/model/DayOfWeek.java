package com.example.adamkovar.dp.model;

public enum DayOfWeek {

    MONDAY(0,1,"Pondelok"),
    TUESDAY(1,2,"Utorok"),
    WEDNESDAY(2,3, "Streda"),
    THURSDAY(3,4, "Štvrtok"),
    FRIDAY(4,5, "Piatok"),
    SATURDAY(5,6, "Sobota"),
    SUNDAY(6,7, "Nedeľa");

    private Integer id;
    private Integer noInWeek;
    private String name;

    DayOfWeek(Integer id, Integer noInWeek, String name) {
        this.id = id;
        this.noInWeek = noInWeek;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getNoInWeek() {
        return noInWeek;
    }

    public String getName() {
        return name;
    }

    public static DayOfWeek geDayOfWeekById(Integer id) {
        for(DayOfWeek day : DayOfWeek.values()){
            if(day.id == id) {
                return day;
            }
        }
        return null;
    }
}
