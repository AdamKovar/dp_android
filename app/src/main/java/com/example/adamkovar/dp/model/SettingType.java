package com.example.adamkovar.dp.model;

public enum SettingType {

    LOGIN(1),
    CONTACT_PERSON(2),
    MAIL_COUNT(3);

    private int id;

    SettingType(int id) {
        this.id = id;
    }
}
