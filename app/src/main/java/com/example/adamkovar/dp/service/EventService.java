package com.example.adamkovar.dp.service;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.adamkovar.dp.dbHelper.EventDBHelper;
import com.example.adamkovar.dp.model.Event.Event;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EventService {

    public static List<Event> fillEventList(){
        List<Event> events = new ArrayList<>();
        return events;
    }

    public static LocalDate getDateFromEvent(String date) {
        String[] dateValues = date.split("/");
        return LocalDate.of(Integer.parseInt(dateValues[2].trim()),Integer.parseInt(dateValues[0].trim()),
                Integer.parseInt(dateValues[1].trim()));
    }

    public static boolean dateIsValid(LocalDate currentDate, LocalDate startDate, LocalDate endDate) {
        return (currentDate.isBefore(endDate) || currentDate.equals(endDate)) &&
                (currentDate.isAfter(startDate) || currentDate.equals(startDate));
    }

    public static List<Event> getEventsByDay(LocalDate currentDate , String day, Context context) {
        EventDBHelper eventDBHelper = new EventDBHelper(context);
        List<Event> allEventList = eventDBHelper.getAllEvents();
        if(allEventList == null || allEventList.isEmpty()) {
            return new ArrayList<>();
        }
        List<Event> selectedEvents = new ArrayList<>();
        for (Event event : allEventList) {
            LocalDate startDate = getDateFromEvent(event.getValidFrom());
            LocalDate endDate = getDateFromEvent(event.getValidTo());
            if (EventService.dateIsValid(currentDate, startDate, endDate)) {
                if (day.toLowerCase().equals(event.getDay().toLowerCase())) {
                    selectedEvents.add(event);
                }
            }
        }
        return selectedEvents;
    }


    public static List<Event> getEventsByDay(CalendarDay calendarDay, String day, Context context) {
        EventDBHelper eventDBHelper = new EventDBHelper(context);
        List<Event> allEventList = eventDBHelper.getAllEvents();
        List<Event> selectedEvents = new ArrayList<>();
        LocalDate currentDate = LocalDate.of(calendarDay.getYear(), calendarDay.getMonth(),
                calendarDay.getDay());
        if(allEventList == null || allEventList.isEmpty()) {
            return new ArrayList<>();
        }
        for (Event event : allEventList) {
            LocalDate startDate = EventService.getDateFromEvent(event.getValidFrom());
            LocalDate endDate = EventService.getDateFromEvent(event.getValidTo());
            if (EventService.dateIsValid(currentDate, startDate, endDate)) {
                if (day.toLowerCase().equals(event.getDay().toLowerCase())) {
                    selectedEvents.add(event);
                }
            }
        }
        return selectedEvents;
    }

}
