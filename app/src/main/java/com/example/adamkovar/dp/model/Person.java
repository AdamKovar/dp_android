package com.example.adamkovar.dp.model;

import java.io.Serializable;

public class Person implements Serializable {

    private String id;
    private String name;
    private String aisId;
    private String login;
    private String title;
    private String dateOfBirth;
    private String gender;
    private String mail;

    public Person(String id, String name, String aisId, String login, String title,
                  String dateOfBirth, String gender, String mail) {
        this.id = id;
        this.name = name;
        this.aisId = aisId;
        this.login = login;
        this.title = title;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.mail = mail;
    }

    public Person() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAisId() {
        return aisId;
    }

    public void setAisId(String aisId) {
        this.aisId = aisId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", aisId='" + aisId + '\'' +
                '}';
    }
}

