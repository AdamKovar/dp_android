package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.example.adamkovar.dp.model.Event.Event;

import java.util.ArrayList;
import java.util.List;

public class EventDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String EVENT_TABLE_NAME = "event";
    public static final String EVENT_COLUMN_ID = "id";
    public static final String EVENT_COLUMN_DAY = "day";
    public static final String EVENT_COLUMN_NAME = "name";
    public static final String EVENT_COLUMN_COURSE_NO = "courseNo";
    public static final String EVENT_COLUMN_START = "startDate";
    public static final String EVENT_COLUMN_END = "endDate";
    public static final String EVENT_COLUMN_ROOM = "room";
    public static final String EVENT_COLUMN_TEACHER = "teacher";
    public static final String EVENT_COLUMN_EVENT_TYPE = "eventType";
    public static final String EVENT_COLUMN_VALID_FROM = "validFrom";
    public static final String EVENT_COLUMN_VALID_TO = "validTo";
    public static final String EVENT_COLUMN_ORIGIN = "origin";
    public static final String EVENT_COLUMN_EVENT_PERIODICITY = "eventPeriodicity";
    public static final String EVENT_COLUMN_LOGIN = "login";

    public boolean insertEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformEventToDb(event);
        db.insert(EVENT_TABLE_NAME, null, contentValues);
        return true;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS event");
        onCreate(db);
    }

    public EventDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public boolean updateEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformEventToDb(event);
        db.update(EVENT_TABLE_NAME, contentValues, "id = ? ",
                new String[]{event.getId()});
        return true;
    }

    public List<Event> getAllEvents() {
        List<Event> events = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from event", null);
        if (res.getCount() <= 0) {
            return null;
        }
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(EVENT_COLUMN_ID)) != null) {
                Event event = getEventFromDB(res);
                events.add(event);
            }
            res.moveToNext();
        }
        return events;
    }

    public List<Event> getAllEventsByLogin(String login) {
        List<Event> events = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from event where login= '" + login + "'", null);
        if (res.getCount() <= 0) {
            return null;
        }
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(EVENT_COLUMN_ID)) != null) {
                Event event = getEventFromDB(res);
                events.add(event);
            }
            res.moveToNext();
        }
        return events;
    }

    public Event getEventByLoginAncCourseNoAndValid(String login, String courseNo, String type, String validFrom, String validTo) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from event where login= '" + login +
                "' and courseNo = '" + courseNo + "' and eventType = '" + type + "' and validFrom = '" + validFrom +
                "' and validTo = '" + validTo + "'", null);
        if (res.getCount() <= 0) {
            return null;
        }
        res.moveToFirst();
        return getEventFromDB(res);
    }


    private Event getEventFromDB(Cursor res) {
        Event event = new Event(res.getString(res.getColumnIndex(EVENT_COLUMN_ID)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_DAY)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_NAME)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_COURSE_NO)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_START)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_END)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_ROOM)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_TEACHER)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_EVENT_TYPE)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_VALID_FROM)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_VALID_TO)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_ORIGIN)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_EVENT_PERIODICITY)),
                res.getString(res.getColumnIndex(EVENT_COLUMN_LOGIN))
        );
        return event;
    }

    private ContentValues transformEventToDb(Event event) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EVENT_COLUMN_DAY, event.getDay());
        contentValues.put(EVENT_COLUMN_NAME, event.getName());
        contentValues.put(EVENT_COLUMN_COURSE_NO, event.getCourseNo());
        contentValues.put(EVENT_COLUMN_START, event.getStart());
        contentValues.put(EVENT_COLUMN_END, event.getEnd());
        contentValues.put(EVENT_COLUMN_ROOM, event.getRoom());
        contentValues.put(EVENT_COLUMN_TEACHER, event.getTeacher());
        contentValues.put(EVENT_COLUMN_EVENT_TYPE, event.getEventType());
        contentValues.put(EVENT_COLUMN_VALID_FROM, event.getValidFrom());
        contentValues.put(EVENT_COLUMN_VALID_TO, event.getValidTo());
        contentValues.put(EVENT_COLUMN_ORIGIN, event.getOrigin());
        contentValues.put(EVENT_COLUMN_EVENT_PERIODICITY, event.getEventPeriodicity());
        contentValues.put(EVENT_COLUMN_LOGIN, event.getLogin());
        return contentValues;
    }
}
