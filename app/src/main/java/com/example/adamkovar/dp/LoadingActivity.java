package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.adamkovar.dp.dbHelper.CourseDBHelper;
import com.example.adamkovar.dp.dbHelper.EventDBHelper;
import com.example.adamkovar.dp.dbHelper.FacultyContactDBHelper;
import com.example.adamkovar.dp.dbHelper.FacultyContactPersonDBHelper;
import com.example.adamkovar.dp.dbHelper.FacultyOpenHourDBHelper;
import com.example.adamkovar.dp.dbHelper.PersonDBHelper;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.Course.Course;
import com.example.adamkovar.dp.model.Event.Event;
import com.example.adamkovar.dp.model.Event.EventPeriodicity;
import com.example.adamkovar.dp.model.Faculty.FacultyContact;
import com.example.adamkovar.dp.model.Faculty.FacultyContactPerson;
import com.example.adamkovar.dp.model.Faculty.FacultyOpenHour;
import com.example.adamkovar.dp.model.Faculty.FacultyType;
import com.example.adamkovar.dp.model.Person;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoadingActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "PrefsFile";

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        String login = (String) getIntent().getSerializableExtra("login");
        loadAllData(login);
    }

    private void loadAllData(String login) {
        getCoursesInfo(this, login);

    }

    public static void changeActivity(Context context, String login) {
        PersonDBHelper dbHelper = new PersonDBHelper(context);
        Person savedPerson = dbHelper.getPersonByLogin(login);
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("Person", savedPerson);
        context.startActivity(intent);
    }

    public static void getCoursesInfo(Context context, String login) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "study/info?login=" + login;
        Log.d("url: ", url);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            List<Course> courses = parseCourseJsonArray(response);
                            CourseDBHelper dbHelper = new CourseDBHelper(context);
                            updateCourses(dbHelper, courses);
                            getFacultyInfo(context, login);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Courses loading failed!",
                        Toast.LENGTH_SHORT).show();
                getFacultyInfo(context, login);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public static void getFacultyInfo(Context context, String login) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "faculty/info/all?login=" + login;
        Log.d("url: ", url);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            List<FacultyContact> contacts = parseFacultyContactJsonArray(response);
                            FacultyContactDBHelper dbHelper = new FacultyContactDBHelper(context);
                            updateFacultyContact(dbHelper, contacts);
                            getEventsInfo(context, login);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Faculty contact loading failed!",
                        Toast.LENGTH_SHORT).show();
                getEventsInfo(context, login);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    public static List<Course> parseCourseJsonArray(JSONArray jsonArray) {
        List<Course> courses = new ArrayList<>();
        for (int i = 0; i <= jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                Course course = new Course(
                        null,
                        object.getString("name"),
                        object.getString("reference"),
                        object.getString("courseNumber"),
                        object.getString("type"),
                        object.getString("language"),
                        object.getString("examType"),
                        object.getString("examAttempt"),
                        object.getString("examMark"),
                        object.getString("credits"),
                        object.getString("semesterType"),
                        object.getString("period"),
                        object.getString("faculty"),
                        object.getString("login"),
                        100,
                        56,
                        100,
                        56
                );
                courses.add(course);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return courses;
    }

    public static void updateCourses(CourseDBHelper dbHelper, List<Course> courses) {
        for (Course course : courses) {
            dbHelper.getAllTableName();
            int rowsNo = dbHelper.numberOfRows();
            Course savedCourse = dbHelper.getCourseByUserIdAndCourseNumber
                    (course.getLogin(), course.getCode());
            if (savedCourse != null) {
                course.setId(savedCourse.getId());
                dbHelper.updateCourse(course);
            } else {
                dbHelper.insertCourse(course);
            }

        }
    }

    public static List<FacultyContact> parseFacultyContactJsonArray(JSONArray jsonArray) {
        List<FacultyContact> contacts = new ArrayList<>();
        for (int i = 0; i <= jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                FacultyContact contact = new FacultyContact(
                        null,
                        object.getString("faculty"),
                        object.getString("name"),
                        object.getString("address"),
                        object.getString("phone"),
                        object.getString("fax"),
                        object.getString("mail"),
                        object.getString("note")
                );
                contacts.add(contact);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return contacts;
    }

    public static void updateFacultyContact(FacultyContactDBHelper dbHelper, List<FacultyContact> contacts) {
        for (FacultyContact contact : contacts) {
            FacultyContact savedContact = dbHelper.getFacultyContactByFaculty(contact.getFaculty());
            if (savedContact != null) {
                contact.setId(savedContact.getId());
                dbHelper.updateFacultyContact(contact);
            } else {
                dbHelper.insertFacultyContact(contact);
            }

        }
    }

    public static void getEventsInfo(Context context, String login) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "timetable?login=" + login;
        Log.d("url: ", url);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            List<Event> courses = parseEventJsonArray(response);
                            EventDBHelper eventDBHelper = new EventDBHelper(context);
                            updateEvent(eventDBHelper, courses);
                            getFacultyOpenHopursByFaculty(context, login);
                            changeActivity(context, login);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Event loading failed!",
                        Toast.LENGTH_SHORT).show();
                getFacultyOpenHopursByFaculty(context, login);
                changeActivity(context, login);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    public static List<Event> parseEventJsonArray(JSONArray jsonArray) {
        List<Event> courses = new ArrayList<>();
        for (int i = 0; i <= jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                Event course = new Event(
                        null,
                        object.getString("day"),
                        object.getString("name"),
                        object.getString("courseNo"),
                        object.getString("start"),
                        object.getString("end"),
                        object.getString("room"),
                        object.getString("teacher"),
                        object.getString("eventType"),
                        object.getString("validFrom"),
                        object.getString("validTo"),
                        "true",
                        EventPeriodicity.ONCEPERWEEK.name(),
                        object.getString("login")
                );
                courses.add(course);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return courses;
    }

    public static void updateEvent(EventDBHelper dbHelper, List<Event> events) {
        for (Event event : events) {
            Event savedEvent = dbHelper.getEventByLoginAncCourseNoAndValid
                    (event.getLogin(), event.getCourseNo(), event.getEventType(), event.getValidFrom(), event.getValidTo());
            if (savedEvent != null) {
                event.setId(savedEvent.getId());
                dbHelper.updateEvent(event);
            } else {
                dbHelper.insertEvent(event);
            }

        }
    }

    public static void getFacultyOpenHopursByFaculty(Context context, String login) {
        for(FacultyType faculty : FacultyType.values()) {
            RequestQueue queue = Volley.newRequestQueue(context);
            String url = BASE_URL + "faculty/openHours?login=" + login + "&faculty=" + faculty.getId().toString();
            Log.d("url: ", url);
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                Log.d("response", response.toString());
                                List<FacultyOpenHour> openHours = parseFacultyOpenHoursJsonArray(response);
                                FacultyOpenHourDBHelper dbHelper = new FacultyOpenHourDBHelper(context);
                                updateFacultyOpenHour(dbHelper, openHours);
                                getFacultyContacPersosonByFaculty(context, login, faculty.getId().toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(context, "Courses loading failed!",
                            Toast.LENGTH_SHORT).show();
                }
            });
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }


    public static void getFacultyContacPersosonByFaculty(Context context, String login, String faculty) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "faculty/contactPeople?login=" + login + "&faculty=" + faculty;
        Log.d("url: ", url);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            List<FacultyContactPerson> contacts = parseFacultyContacPersonJsonArray(response);
                            FacultyContactPersonDBHelper dbHelper = new FacultyContactPersonDBHelper(context);
                            updateFacultyContactPerson(dbHelper, contacts);
                            getFacultyViceDeanContactByFaculty(context, login, faculty);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Courses loading failed!",
                        Toast.LENGTH_SHORT).show();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void getFacultyViceDeanContactByFaculty(Context context, String login, String faculty) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = BASE_URL + "faculty/contactViceDean?login=" + login + "&faculty=" + faculty;
        Log.d("url: ", url);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("response", response.toString());
                            List<FacultyContactPerson> contacts = parseFacultyContacPersonJsonArray(response);
                            FacultyContactPersonDBHelper dbHelper = new FacultyContactPersonDBHelper(context);
                            updateFacultyContactPerson(dbHelper, contacts);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    public static List<FacultyContactPerson> parseFacultyContacPersonJsonArray(JSONArray jsonArray) {
        List<FacultyContactPerson> contactPersonList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                FacultyContactPerson facultyContactPerson = new FacultyContactPerson(
                        null,
                        object.getString("faculty"),
                        object.getString("personType"),
                        object.getString("aisId"),
                        object.getString("name"),
                        object.getString("room"),
                        object.getString("phone"),
                        object.getString("mail"),
                        object.getString("note")
                );
                contactPersonList.add(facultyContactPerson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return contactPersonList;
    }

    public static void updateFacultyContactPerson(FacultyContactPersonDBHelper dbHelper, List<FacultyContactPerson> contactPersonList) {
        for (FacultyContactPerson contactPerson : contactPersonList) {
            FacultyContactPerson savedContactPerson = dbHelper.getFacultyContactPersonListByFacultyAndAisId(contactPerson.getFaculty(), contactPerson.getAisId());
            if (savedContactPerson != null) {
                contactPerson.setId(savedContactPerson.getId());
                dbHelper.updateFacultyContactPerson(contactPerson);
            } else {
                dbHelper.insertFacultyContactPerson(contactPerson);
            }
        }
    }


    public static List<FacultyOpenHour> parseFacultyOpenHoursJsonArray(JSONArray jsonArray) {
        List<FacultyOpenHour> facultyOpenHours = new ArrayList<>();
        for (int i = 0; i <= jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                FacultyOpenHour facultyOpenHour = new FacultyOpenHour(
                        null,
                        object.getString("faculty"),
                        object.getString("dayOfWeek"),
                        object.getString("from"),
                        object.getString("to"),
                        object.getString("note")
                );
                facultyOpenHours.add(facultyOpenHour);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return facultyOpenHours;
    }

    public static void updateFacultyOpenHour(FacultyOpenHourDBHelper dbHelper, List<FacultyOpenHour> facultyOpenHours) {
        for (FacultyOpenHour facultyOpenHour : facultyOpenHours) {
            FacultyOpenHour savedFacultyOpenHour = dbHelper.getAllFacultyOpenHourListByFacultyAndDayOfWeek(facultyOpenHour.getFaculty(), facultyOpenHour.getDayOfWeek());
            if (savedFacultyOpenHour != null) {
                facultyOpenHour.setId(savedFacultyOpenHour.getId());
                dbHelper.updateFacultyOpenHour(facultyOpenHour);
            } else {
                dbHelper.insertFacultyOpenHour(facultyOpenHour);
            }
        }
    }


}
