package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.Faculty.FacultyContactPerson;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;
import com.example.adamkovar.dp.service.ConnectionService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FacultyContactPersonAdapter extends ArrayAdapter<FacultyContactPerson> {

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";

    Context context;


    private List<FacultyContactPerson> contactPeople = new ArrayList<>();

    public FacultyContactPersonAdapter(Context c, List<FacultyContactPerson> contactPeople) {
        super(c, R.layout.faculty_contact_item);
        this.context = c;
        this.contactPeople = contactPeople;
    }

    public FacultyContactPersonAdapter(Context c) {
        super(c, R.layout.faculty_contact_item);
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.faculty_contact_item, parent, false);
        ImageView personImage = view.findViewById(R.id.faculty_contact_image);
        TextView contactName = view.findViewById(R.id.faculty_contact_name);
        TextView contactRoom = view.findViewById(R.id.faculty_contact_room);
        TextView contactPhone = view.findViewById(R.id.faculty_contact_phone_value);
        TextView contactMail = view.findViewById(R.id.faculty_contact_mail_value);
        TextView contactNote = view.findViewById(R.id.faculty_contact_note_value);
        ImageView priorityIcon = view.findViewById(R.id.faculty_contact_primary_icon);
        personImage.setImageResource(R.drawable.portrait);
        SettingsDBHelper settingsDBHelper = new SettingsDBHelper(getContext());
        Setting settingLogin = settingsDBHelper.getSettingByType(SettingType.LOGIN);
        if(settingLogin != null) {
            if (ConnectionService.isNetworkConnectedBoolean(getContext())) {
                Picasso.get().load(BASE_URL.concat("person/picture/" + contactPeople.get(position).getAisId() +
                        "?login=" + settingLogin.getValue())).into(personImage);
            }
        }
        if(!contactPeople.get(position).getName().equals("null")) {
            contactName.setText(contactPeople.get(position).getName());
        }
        if(!contactPeople.get(position).getRoom().equals("null")) {
            contactRoom.setText(contactPeople.get(position).getRoom());
        }
        if(!contactPeople.get(position).getPhone().equals("null")) {
            contactPhone.setText(contactPeople.get(position).getPhone());
        }
        if(!contactPeople.get(position).getMail().equals("null")) {
            contactMail.setText(contactPeople.get(position).getMail());
        }
        if(!contactPeople.get(position).getNote().equals("null")) {
            contactNote.setText(contactPeople.get(position).getNote());
        }

        Setting settingContactPerson = settingsDBHelper.getSettingByType(SettingType.CONTACT_PERSON);
        if (settingContactPerson != null) {
            if (settingContactPerson.getValue() != null) {
                if (settingContactPerson.getValue().equals(contactPeople.get(position).getAisId())) {
                    priorityIcon.setVisibility(View.VISIBLE);
                }
            }
        }
        if(!ConnectionService.isNetworkConnectedBoolean(context)) {
            personImage.setImageResource(R.drawable.portrait);
        }
        return view;
    }


    @Override
    public int getCount() {
        return contactPeople.size();
    }

    public List<FacultyContactPerson> getContactPeople() {
        return contactPeople;
    }

    public void setContactPeople(List<FacultyContactPerson> contactPeople) {
        this.contactPeople = contactPeople;
    }
}
