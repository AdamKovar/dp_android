package com.example.adamkovar.dp.model.Faculty;

public class FacultyContactPerson {

    private String id;
    private String faculty;
    private String personType;
    private String aisId;
    private String name;
    private String room;
    private String phone;
    private String mail;
    private String note;

    public FacultyContactPerson(String id, String faculty, String personType, String aisId,
                                String name, String room, String phone, String mail, String note) {
        this.id = id;
        this.faculty = faculty;
        this.personType = personType;
        this.aisId = aisId;
        this.name = name;
        this.room = room;
        this.phone = phone;
        this.mail = mail;
        this.note = note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getAisId() {
        return aisId;
    }

    public void setAisId(String aisId) {
        this.aisId = aisId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
