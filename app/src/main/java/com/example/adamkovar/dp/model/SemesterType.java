package com.example.adamkovar.dp.model;

public enum SemesterType {
    SEMESTER_TYPE_WS(0, "Zimný", "Winter"),
    SEMESTER_TYPE_SS(1, "Letný", "Summer");

    private int id;

    private String nameSK;

    private String nameEN;

    SemesterType(int id, String nameSK, String nameEN) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
    }

    public static SemesterType getSemesterType(String type) {
        return SemesterType.valueOf(type);
    }

    public static String getSemesterTypeById(int id){
        for(SemesterType type : SemesterType.values()){
            if(type.id == id){
                return type.name();
            }
        }
        return "";
    }
}
