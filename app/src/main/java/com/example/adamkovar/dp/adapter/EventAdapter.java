package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.dbHelper.EventNotificationDBHelper;
import com.example.adamkovar.dp.model.Event.Event;
import com.example.adamkovar.dp.model.EventNotification;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class EventAdapter extends ArrayAdapter<Event> {
    Context context;
    List<Event> eventList;
    CalendarDay calendarDay;


    public EventAdapter(Context c, List<Event> eventList, CalendarDay calendarDay ) {
        super(c, R.layout.today_activity_row, R.id.today_activity_title);
        this.context = c;
        this.eventList = eventList;
        this.calendarDay = calendarDay;
    }

    public EventAdapter(Context c) {
        super(c, R.layout.today_activity_row);
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = layoutInflater.inflate(R.layout.today_activity_row, parent, false);
        TextView titleText = row.findViewById(R.id.today_activity_title);
        TextView descriptionText = row.findViewById(R.id.today_activity_description);
        TextView startDateText = row.findViewById(R.id.today_activity_start_date);
        TextView endDateText = row.findViewById(R.id.today_activity_end_date);
        TextView eventTypeText = row.findViewById(R.id.today_activity_type);
        TextView eventRoom = row.findViewById(R.id.today_activity_room);
        ImageButton notifyButton = row.findViewById(R.id.createNotificationBtn);
        notifyButton.setFocusable(false);

        if (!eventList.isEmpty()) {
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");//formating according to my need
            String startDate = eventList.get(position).getStart().replace(".", ":");
            String endDate = eventList.get(position).getEnd().replace(".", ":");

            titleText.setText(eventList.get(position).getName());
            descriptionText.setText(eventList.get(position).getTeacher());
            startDateText.setText(startDate.replace(".", ":"));
            endDateText.setText(endDate.replace(".", ":"));
            eventTypeText.setText(eventList.get(position).getEventType());
            eventRoom.setText(eventList.get(position).getRoom()
                    .substring(0, eventList.get(position).getRoom().indexOf("(")));

            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            int minute = Calendar.getInstance().get(Calendar.MINUTE);
            LocalDateTime currentDateTime = LocalDateTime.of(year, month, day, hour, minute);

            LocalDateTime lectureStartDate = LocalDateTime.of(year, month, day,
                    getHour(eventList.get(position).getStart()),
                    getMinute(eventList.get(position).getStart()));
            LocalDateTime lectureEndDate = LocalDateTime.of(year, month, day,
                    getHour(eventList.get(position).getEnd()),
                    getMinute(eventList.get(position).getEnd()));
            if (eventList.get(position).getDay().equals(LocalDate.now().getDayOfWeek().name())) {
                if (currentDateTime.isAfter(lectureStartDate) && currentDateTime.isBefore(lectureEndDate)) {
                    row.setBackgroundColor(Color.parseColor("#E6E6E6"));
                }
            }

            Event event = eventList.get(position);
            String date = makeDateString(calendarDay.getYear(), calendarDay.getMonth(), calendarDay.getDay());
            if(hasEventNotification(event.getName(), date, event.getStart())) {
                notifyButton.setVisibility(View.VISIBLE);
            } else {
                notifyButton.setVisibility(View.GONE);
            }
        }

        notifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Notify", "click");
            }
        });


        return row;
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @NonNull
    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private int getHour(String date) {
        return Integer.parseInt(date.replace("\n", "").split("\\.")[0]);
    }

    private int getMinute(String date) {
        return Integer.parseInt(date.split("\\.")[1]);
    }

    private boolean hasEventNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if(notifications != null) {
            if(notifications.isEmpty()) {
                return false;
            }else {
                return true;
            }
        }
        return false;
    }

    private String makeDateString(Integer year, Integer month, Integer day) {
        StringBuilder builder = new StringBuilder();
        builder.append(year).append(".");
        builder.append(month).append(".");
        builder.append(day).append(".");
        return builder.toString();
    }

}