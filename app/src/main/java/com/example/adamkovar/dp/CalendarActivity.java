package com.example.adamkovar.dp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.adamkovar.dp.adapter.EventAdapter;
import com.example.adamkovar.dp.dbHelper.EventDBHelper;
import com.example.adamkovar.dp.dbHelper.EventNotificationDBHelper;
import com.example.adamkovar.dp.model.CalendarInfo;
import com.example.adamkovar.dp.model.DayOfWeek;
import com.example.adamkovar.dp.model.Event.Event;
import com.example.adamkovar.dp.model.EventNotification;
import com.example.adamkovar.dp.service.EventService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class CalendarActivity extends AppCompatActivity {

    SwipeMenuListView currentDayActivities;
    ImageView eventNoDataImage;
    List<Event> eventList;
    FloatingActionButton actionButton;
    private static final String[] times = {"15 min", "30 min", "1 h", "2h", "1d"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        actionButton = (FloatingActionButton) findViewById(R.id.newEventBtn);
        eventNoDataImage = findViewById(R.id.no_event_data);

        MaterialCalendarView calendarView = (MaterialCalendarView) findViewById(R.id.calendarView);
        calendarView.setSelectedDate(CalendarDay.today());
        this.eventList = EventService.getEventsByDay(LocalDate.now(), LocalDate.now().getDayOfWeek().name(), this);

        currentDayActivities = findViewById(R.id.daily_event_list_view);

        if (!eventList.isEmpty()) {
            EventAdapter eventAdapter = new EventAdapter(this, eventList, calendarView.getSelectedDate());
            currentDayActivities.setAdapter(eventAdapter);
            currentDayActivities.setOnItemClickListener((parent, view, position, id) -> {
                Log.d("click", String.valueOf(position));
                Event event = eventList.get(position);
                openEventDetail(event);
            });
            currentDayActivities.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {

                    Log.v("long clicked", "pos: " + pos);
                    Event event = eventList.get(pos);
                    String date = makeDateString(CalendarDay.today().getYear(), CalendarDay.today().getMonth(), CalendarDay.today().getDay());
                    if (hasEventNotification(event.getName(), date, event.getStart())) {
                        cancelAlarm(event, date);
                        removeNotification(event.getName(), date, event.getStart());
                        EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList, CalendarDay.today());
                        currentDayActivities.setAdapter(eventAdapter);
                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(CalendarActivity.this);
                        builder.setTitle(getApplicationContext().getResources().getString(R.string.set_time_before_notification));
                        int timePosition = 0;
// add a list
                        int checkedItem = 0; // cow
                        builder.setSingleChoiceItems(times, checkedItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // user checked an item
                                int timePosition = which;
                            }
                        });
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addNotification(event.getName(), date, event.getStart());
                                setNotification(event, date, timePosition);
                                EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList, CalendarDay.today());
                                currentDayActivities.setAdapter(eventAdapter);
                            }
                        });
                        builder.setNegativeButton("Cancel", null);

// create an

// create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                                        .setTextColor(getApplicationContext()
                                                .getColor(R.color.secondaryTextColor));
                                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                        .setTextColor(getApplicationContext()
                                                .getColor(R.color.colorAccent));
                            }
                        });
                        dialog.show();



                    }


                    //setNotification(event);
                    return true;
                }
            });
            currentDayActivities.setOnScrollListener(new AbsListView.OnScrollListener() {
                private int currentVisibleItemCount;
                private int currentScrollState;
                private int currentFirstVisibleItem;
                private int totalItem;
                private LinearLayout lBelow;


                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    // TODO Auto-generated method stub
                    this.currentScrollState = scrollState;
                    this.isScrollCompleted();
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    // TODO Auto-generated method stub
                    this.currentFirstVisibleItem = firstVisibleItem;
                    this.currentVisibleItemCount = visibleItemCount;
                    this.totalItem = totalItemCount;


                }

                private void isScrollCompleted() {
                    if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                            && this.currentScrollState == SCROLL_STATE_IDLE) {
                        /** To do code here*/
                        Log.d("scrol", "up");

                    }
                }
            });
        } else {
            currentDayActivities.setEmptyView(eventNoDataImage);
        }
        currentDayActivities.setVisibility(eventList.size() > 0 ? View.VISIBLE : View.GONE);
        eventNoDataImage.setVisibility(eventList.size() > 0 ? View.GONE : View.VISIBLE);
        calendarView.setSelectedDate(CalendarDay.today());
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                changeDailyEvents(calendarDay);

            }
        });

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // openEventDetail();
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(R.color.colorAccentErr);
                // set item width
                deleteItem.setWidth(162);
                // set a icon
                deleteItem.setIcon(R.drawable.baseline_delete_forever_white_1_48dp);
                // add to menu
                SwipeMenuItem exportItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                exportItem.setBackground(R.color.colorPrimary);
                // set item width
                exportItem.setWidth(170);
                // set a icon
                exportItem.setIcon(R.drawable.baseline_share_white_48dp);
                // add to menu

                menu.addMenuItem(exportItem);
                //menu.addMenuItem(deleteItem);
            }
        };

        currentDayActivities.setMenuCreator(creator);

        currentDayActivities.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        Event event = eventList.get(index);
                        List<Event> events = new ArrayList<>();
                        events.add(event);
                        exportAllEvents(events);
                        break;

                    case 1:
                        Log.d("click", "onMenuItemClick: clicked item " + index);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

    }

    public void changeDailyEvents(CalendarDay calendarDay) {
        LocalDate currentDate = LocalDate.of(calendarDay.getYear(), calendarDay.getMonth(),
                calendarDay.getDay());
        switch (currentDate.getDayOfWeek()) {
            case MONDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "MONDAY", getApplicationContext());
                Log.d("Day", "MONDAY");
                break;
            case TUESDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "TUESDAY", getApplicationContext());
                Log.d("Day", "TUESDAY");
                break;
            case WEDNESDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "WEDNESDAY", getApplicationContext());
                Log.d("Day", "WEDNESDAY");
                break;
            case THURSDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "THURSDAY", getApplicationContext());
                Log.d("Day", "THURSDAY");
                break;
            case FRIDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "FRIDAY", getApplicationContext());
                Log.d("Day", "FRIDAY");
                break;
            case SATURDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "SATURDAY", getApplicationContext());
                Log.d("Day", "SATURDAY");
                break;
            case SUNDAY:
                this.eventList = EventService.getEventsByDay(calendarDay, "SUNDAY", getApplicationContext());
                Log.d("Day", "SUNDAY");
            default:
                break;
        }
        if (!eventList.isEmpty()) {
            EventAdapter eventAdapter = new EventAdapter(this, eventList, calendarDay);
            currentDayActivities.setAdapter(eventAdapter);
//            ((BaseAdapter) currentDayActivities.getAdapter()).notifyDataSetChanged();
            currentDayActivities.setEmptyView(eventNoDataImage);
            currentDayActivities.setOnItemClickListener((parent, view, position, id) -> {
                Log.d("click", String.valueOf(position));
                Event event = eventList.get(position);
                openEventDetail(event);
            });
            currentDayActivities.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {

                    Log.v("long clicked", "pos: " + pos);
                    Event event = eventList.get(pos);
                    String date = makeDateString(calendarDay.getYear(), calendarDay.getMonth(), calendarDay.getDay());
                    if (hasEventNotification(event.getName(), date, event.getStart())) {
                        cancelAlarm(event, date);
                        removeNotification(event.getName(), date, event.getStart());
                        EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList, calendarDay);
                        currentDayActivities.setAdapter(eventAdapter);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CalendarActivity.this);
                        builder.setTitle(getApplicationContext().getResources().getString(R.string.set_time_before_notification));
                        int timePosition = 0;
// add a list
                        int checkedItem = 0; // cow
                        builder.setSingleChoiceItems(times, checkedItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // user checked an item
                                int timePosition = which;
                            }
                        });
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                addNotification(event.getName(), date, event.getStart());
                                setNotification(event, date, timePosition);
                                EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList, calendarDay);
                                currentDayActivities.setAdapter(eventAdapter);
                            }
                        });
                        builder.setNegativeButton(getApplicationContext().getText(R.string.dialog_cancel_label), null);

// create an

// create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                                        .setTextColor(getApplicationContext()
                                                .getColor(R.color.secondaryTextColor));
                                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                        .setTextColor(getApplicationContext()
                                                .getColor(R.color.colorAccent));
                            }
                        });
                        dialog.show();


                    }


                    //setNotification(event);
                    return true;
                }
            });

            currentDayActivities.setOnScrollListener(new AbsListView.OnScrollListener() {
                private int currentVisibleItemCount;
                private int currentScrollState;
                private int currentFirstVisibleItem;
                private int totalItem;
                private LinearLayout lBelow;


                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    // TODO Auto-generated method stub
                    this.currentScrollState = scrollState;
                    this.isScrollCompleted();
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    // TODO Auto-generated method stub
                    this.currentFirstVisibleItem = firstVisibleItem;
                    this.currentVisibleItemCount = visibleItemCount;
                    this.totalItem = totalItemCount;
                    this.isScrollCompleted();


                }

                private void isScrollCompleted() {
                    if (totalItem == currentVisibleItemCount) {
                        findViewById(R.id.newEventBtn).setVisibility(View.VISIBLE);
                        return;
                    }
                    if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                            || (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                            && this.currentScrollState == SCROLL_STATE_IDLE)) {
                        /** To do code here*/
                        Log.d("scroll", "up");
                        findViewById(R.id.newEventBtn).setVisibility(View.GONE);

                    } else {
                        Log.d("scroll", "down");
                        findViewById(R.id.newEventBtn).setVisibility(View.VISIBLE);
                    }
                }
            });
        } else {
            currentDayActivities.setEmptyView(eventNoDataImage);
        }
        currentDayActivities.setVisibility(eventList.size() > 0 ? View.VISIBLE : View.GONE);
        eventNoDataImage.setVisibility(eventList.size() > 0 ? View.GONE : View.VISIBLE);

        //currentDayActivities.deferNotifyDataSetChanged();
    }


    public void openEventDetail(Event event) {
        Intent intent = new Intent(this, EventDetailActivity.class);
        intent.putExtra("Event", event);
        startActivity(intent);
    }

    public void openEventDetail() {
        Intent intent = new Intent(this, EventDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calendar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            EventDBHelper dbHelper = new EventDBHelper(getApplicationContext());
            List<Event> allEvents = dbHelper.getAllEvents();
            exportAllEvents(allEvents);
        }

        return super.onOptionsItemSelected(item);
    }

    public void exportAllEvents(List<Event> events) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALENDAR)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        1);
            }
        } else {
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_CALENDAR)) {
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_CALENDAR},
                        1);
            }
        } else {
            CalendarInfo calendarInfo = getCalendar(getApplicationContext());
            // Permission has already been granted
            //calendarInfo.setId(1);

            for (Event event : events) {
                exportEvent(event, calendarInfo);
            }
            Toast.makeText(this, getApplicationContext().getString(R.string.event_export_seccessful).concat(" ").concat(calendarInfo.getOwner()), Toast.LENGTH_LONG).show();
        }
    }

    public String makeNoteForEvent(Event event) {
        return event.getTeacher();
    }

    public CalendarInfo getCalendar(Context c) {

        CalendarInfo info;

        String projection[] = {CalendarContract.Calendars._ID, CalendarContract.Calendars.OWNER_ACCOUNT};

        ContentResolver contentResolver = c.getContentResolver();

        Cursor calCursor = contentResolver.query(CalendarContract.Calendars.CONTENT_URI, projection,
                CalendarContract.Calendars.VISIBLE + " = 1 AND " + CalendarContract.Calendars.IS_PRIMARY + "=1",
                null, CalendarContract.Calendars._ID + " ASC");
        if (calCursor.getCount() <= 0) {
            calCursor = contentResolver.query(CalendarContract.Calendars.CONTENT_URI, projection,
                    CalendarContract.Calendars.VISIBLE + " = 1", null,
                    CalendarContract.Calendars._ID + " ASC");

            if (calCursor.moveToFirst()) {
                do {
                    String calOwner;
                    Integer calID;
                    calOwner = calCursor.getString(calCursor.getColumnIndex(projection[1]));
                    calID = calCursor.getInt(calCursor.getColumnIndex(projection[0]));
                    if (calOwner.contains("@gmail.com")) {
                        return new CalendarInfo(calID, calOwner);
                    }
                } while (calCursor.moveToNext());
            }
            calCursor = contentResolver.query(CalendarContract.Calendars.CONTENT_URI, projection,
                    CalendarContract.Calendars.VISIBLE + " = 1", null,
                    CalendarContract.Calendars._ID + " ASC");
            if (calCursor.moveToFirst()) {
                return new CalendarInfo(calCursor.getInt(calCursor.getColumnIndex(projection[0])),
                        calCursor.getString(calCursor.getColumnIndex(projection[1])));
            }
            return new CalendarInfo(1, "");
        }

        return new CalendarInfo(calCursor.getInt(calCursor.getColumnIndex(projection[0])),
                calCursor.getString(calCursor.getColumnIndex(projection[1])));
    }

    private void setNotification(Event event, String date, int position) {
        String[] notificationDate = date.split("\\.");
        LocalDate startDate = LocalDate.of(Integer.parseInt(notificationDate[0]),
                Integer.parseInt(notificationDate[1]),
                Integer.parseInt(notificationDate[2]));
        Integer statHour = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[0]);
        Integer statMinute = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[1]);
        LocalDateTime start = LocalDateTime.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth(), statHour, statMinute);
        switch (position) {
            case 0:
                start = start.minusMinutes(15);
                break;
            case 1:
                start = start.minusMinutes(30);
                break;
            case 2:
                start = start.minusHours(1);
                break;
            case 3:
                start = start.minusHours(2);
                break;
            case 4:
                start = start.minusDays(1);
                break;
            default:
                break;

        }
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        Integer id = getNotificationId(event.getName(), date, event.getStart());
        intent.putExtra("eventName", event.getName());
        intent.putExtra("eventDesc", createNotificationDescription(event));
        intent.putExtra("notificationID", id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, 0);


        alarmManager.setExact(AlarmManager.RTC_WAKEUP, start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), pendingIntent);
        Toast.makeText(CalendarActivity.this, getApplicationContext().getText(R.string.notification_set_seccesfull), Toast.LENGTH_SHORT).show();


    }

    private void cancelAlarm(Event event, String date) {
        Integer id = getNotificationId(event.getName(), date, event.getStart());
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, 0);

        alarmManager.cancel(pendingIntent);
    }

    private String createNotificationDescription(Event event) {
        return event.getRoom();
    }

    private boolean hasEventNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if (notifications != null) {
            if (notifications.isEmpty()) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private String makeDateString(Integer year, Integer month, Integer day) {
        StringBuilder builder = new StringBuilder();
        builder.append(year).append(".");
        builder.append(month).append(".");
        builder.append(day).append(".");
        return builder.toString();
    }

    private void removeNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if (notifications != null) {
            for (EventNotification notification : notifications) {
                dbHelper.deleteNotification(notification);
            }
        }
    }

    private void addNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        EventNotification notification = new EventNotification();
        notification.setEventName(name);
        notification.setDate(date);
        notification.setStart(start);
        dbHelper.insertNotification(notification);
    }

    private Integer getNotificationId(String name, String date, String start) {
        Integer id = 1;
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if (notifications != null) {
            for (EventNotification notification : notifications) {
                id = Integer.parseInt(notification.getId());
            }
        }
        return id;
    }

    private void exportEvent(Event event, CalendarInfo calendarInfo) {
        if (Boolean.parseBoolean(event.getOrigin())) {
            LocalDate startDate = EventService.getDateFromEvent(event.getValidFrom());
            LocalDate endDate = EventService.getDateFromEvent(event.getValidTo());
            for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
                if (DayOfWeek.valueOf(event.getDay()).getNoInWeek().compareTo(date.getDayOfWeek().getValue()) == 0) {

                    Integer statHour = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[0]);
                    Integer statMinute = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[1]);
                    Integer endHour = Integer.parseInt(event.getEnd().replace("\n", "").split("\\.")[0]);
                    Integer endMinute = Integer.parseInt(event.getEnd().replace("\n", "").split("\\.")[1]);
                    LocalDateTime start = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), statHour, statMinute);
                    LocalDateTime end = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), endHour, endMinute);
                    String[] proj =
                            new String[]{
                                    CalendarContract.Instances.BEGIN,
                                    CalendarContract.Instances.END,
                                    CalendarContract.Instances.TITLE};
                    String title = event.getName();
                    Boolean isDuplicated = false;
                    Cursor cursor =
                            CalendarContract.Instances.query(getContentResolver(), proj,
                                    start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(),
                                    end.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        String eventTitle;
                        if (cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.TITLE)).contains(title)) {
                            isDuplicated = true;
                            continue;
                        }
                        while (cursor.moveToNext()) ;
                        // deal with conflict
                    }

                    if (!isDuplicated) {
                        ContentResolver cr = this.getContentResolver();
                        ContentValues cv = new ContentValues();
                        cv.put(CalendarContract.Events.TITLE, event.getName());
                        cv.put(CalendarContract.Events.DESCRIPTION, makeNoteForEvent(event));
                        cv.put(CalendarContract.Events.EVENT_LOCATION, event.getRoom());
                        cv.put(CalendarContract.Events.DTSTART, start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                        cv.put(CalendarContract.Events.DTEND, end.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                        cv.put(CalendarContract.Events.CALENDAR_ID, calendarInfo.getId());
                        cv.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
                        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, cv);
                    }
                }
            }
        }
    }

}
