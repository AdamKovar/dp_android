package com.example.adamkovar.dp.model;

import java.io.Serializable;

public class EmailMessage implements Serializable {

    private String from;

    private String to;

    private String cc;

    private String subject;

    private String sentDate;

    private String sentTime;

    private String message;

    public EmailMessage() {
    }

    public EmailMessage(String from, String to, String cc, String subject, String sentDate, String sentTime, String message) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.subject = subject;
        this.sentDate = sentDate;
        this.sentTime = sentTime;
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }
}
