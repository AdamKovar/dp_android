package com.example.adamkovar.dp.model.Faculty;

public enum FacultyType {


    FACULTY_SVF(10, "Stavebná fakulta", "Faculty of Civil Engineering", "http://www.svf.stuba.sk"),
    FACULTY_SJF(20, "Strojnícka fakulta", "Faculty of Mechanical Engineering", "http://www.sjf.stuba.sk"),
    FACULTY_FEI(30, "Fakulta elektrotechniky a informatiky", "Faculty of Electrical Engineering and Information Technology", "http://www.fei.stuba.sk"),
    FACULTY_FCHPT(40, "Fakulta chemickej a potravinárskej technológie", "Faculty of Chemical and Food Technology", "http://www.fchpt.stuba.sk"),
    FACULTY_FA(50, "Fakulta architektúry", "Faculty of Architecture", "http://www.fa.stuba.sk"),
    FACULTY_MTF(60, "Materiálovotechnologická fakulta so sídlom v Trnave", "Faculty of Materials Science and Technology in Trnava", "http://www.mtf.stuba.sk"),
    FACULTY_FIIT(70, "Fakulta informatiky a informačných technológií", "Faculty of Informatics and Information Technologies", "http://www.fiit.stuba.sk/"),
    FACULTY_STU(232, "Ústav manažmentu STU", "Institute of Management of the STU", "https://www.stuba.sk/");


    private Integer id;
    private String nameSK;
    private String nameEN;
    private String url;

    FacultyType(Integer id, String nameSK, String nameEN, String url) {
        this.id = id;
        this.nameSK = nameSK;
        this.nameEN = nameEN;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public String getNameSK() {
        return nameSK;
    }

    public String getNameEN() {
        return nameEN;
    }

    public String getUrl() {
        return url;
    }

    public static FacultyType findById(Integer id) {
        for(FacultyType type : FacultyType.values()){
            if(type.id.compareTo(id) == 0) {
                return type;
            }
        }
        return null;
    }

    public static FacultyType findByPosittion(Integer position) {
        try {
            return FacultyType.values()[position];
        } catch (Exception e) {
            return null;
        }
    }
}
