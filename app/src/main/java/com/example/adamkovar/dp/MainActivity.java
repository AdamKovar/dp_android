package com.example.adamkovar.dp;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import com.example.adamkovar.dp.adapter.EventAdapter;
import com.example.adamkovar.dp.dbHelper.EventNotificationDBHelper;
import com.example.adamkovar.dp.dbHelper.FacultyContactPersonDBHelper;
import com.example.adamkovar.dp.dbHelper.PersonDBHelper;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.CalendarInfo;
import com.example.adamkovar.dp.model.DayOfWeek;
import com.example.adamkovar.dp.model.Event.Event;

import com.example.adamkovar.dp.model.EventNotification;
import com.example.adamkovar.dp.model.Faculty.FacultyContactPerson;
import com.example.adamkovar.dp.model.Person;

import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;
import com.example.adamkovar.dp.service.ConnectionService;
import com.example.adamkovar.dp.service.EventService;
import com.google.android.material.navigation.NavigationView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.squareup.picasso.Picasso;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.CalendarContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.nikartm.support.ImageBadgeView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String PREFS_NAME = "PrefsFile";

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";

    private static final String GENDER_FEMALE = "female";

    private static final String[] times = {"15 min", "30 min", "1 h", "2h", "1d"};


    ListView currentDayActivities;
    ImageView eventNoDataImage;
    NavigationView navigationView;
    Button sentMailBtn;
    Button setContactBtn;
    Button logOutBtn;
    TextView userName;
    TextView userAisId;
    TextView actualDate;
    TextView actualTime;
    ImageView profileImage;
    ImageBadgeView imageBadgeView;
    private Person person;
    FacultyContactPerson contactPerson;

    List<Event> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.eventList = new ArrayList<>();
        this.person = (Person) getIntent().getSerializableExtra("Person");
        navigationView = findViewById(R.id.nav_view);
        sentMailBtn = findViewById(R.id.sentMailBtn);
        setContactBtn = findViewById(R.id.setContactBtn);
        logOutBtn = findViewById(R.id.menu_logout);
        userName = navigationView.getHeaderView(0).findViewById(R.id.userName);
        userAisId = navigationView.getHeaderView(0).findViewById(R.id.userAisId);
        eventNoDataImage = findViewById(R.id.no_event_data);
        actualDate = findViewById(R.id.actual_date);
        imageBadgeView = findViewById(R.id.inbox_btn);
        profileImage = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0).findViewById(R.id.userImage);
        if (person.getGender().equals(GENDER_FEMALE)) {
            profileImage.setImageResource(R.drawable.portrait_woman);
        } else {
            profileImage.setImageResource(R.drawable.portrait);
        }
        if (ConnectionService.isNetworkConnectedBoolean(getApplicationContext())) {
            Picasso.get().load(BASE_URL.concat("person/picture/" + person.getAisId() +
                    "?login=" + person.getLogin())).into(profileImage);
        }
        if(!ConnectionService.isNetworkConnectedBoolean(this)) {
            if (person.getGender().equals(GENDER_FEMALE)) {
                profileImage.setImageResource(R.drawable.portrait_woman);
            } else {
                profileImage.setImageResource(R.drawable.portrait);
            }


        }

        Date date = Calendar.getInstance().getTime();
        int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        String weekday = new DateFormatSymbols().getWeekdays()[dayOfWeek];
        DateFormat dateFormat = android.text.format.DateFormat.getLongDateFormat(getApplicationContext());
        actualDate.setText(weekday + " " + dateFormat.format(date));
        updateTodayActivityList();
        setContactData();

        imageBadgeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectionService.isNetworkConnectedBoolean(getApplicationContext())) {
                    Intent intent = new Intent(getApplicationContext(), InboxActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this,getApplicationContext().getString(R.string.connection_required_messag),Toast.LENGTH_SHORT).show();
                }
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(contactPerson == null) {
            sentMailBtn.setVisibility(View.GONE);
            setContactBtn.setVisibility(View.VISIBLE);
        } else {
            setContactBtn.setVisibility(View.GONE);
            sentMailBtn.setVisibility(View.VISIBLE);
        }
        sentMailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{contactPerson.getMail()});
                    i.putExtra(Intent.EXTRA_SUBJECT, "");
                    i.putExtra(Intent.EXTRA_TEXT, "");
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(MainActivity.this, getApplicationContext().getString(R.string.no_email_clients_available), Toast.LENGTH_SHORT).show();
                    }
                }

        });

        setContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FacultyActivity.class);
                startActivity(intent);
            }

        });

        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsDBHelper settingsDBHelper = new SettingsDBHelper(getApplicationContext());
                Setting setting = settingsDBHelper.getSettingByType(SettingType.LOGIN);
                setting.setValue("");
                settingsDBHelper.updateSetting(setting);
                SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("isLogged", false);
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onRestart() {
        updateTodayActivityList();
        setContactData();
        super.onRestart();
        if(contactPerson == null) {
            sentMailBtn.setVisibility(View.GONE);
            setContactBtn.setVisibility(View.VISIBLE);
        } else {
            setContactBtn.setVisibility(View.GONE);
            sentMailBtn.setVisibility(View.VISIBLE);
        }
    }



    @Override
    protected void onStart() {
        updateTodayActivityList();
        setContactData();
        super.onStart();
        if(contactPerson == null) {
            sentMailBtn.setVisibility(View.GONE);
            setContactBtn.setVisibility(View.VISIBLE);
        } else {
            setContactBtn.setVisibility(View.GONE);
            sentMailBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Boolean actionComplete = true;

        if (id == R.id.menu_calendar) {

            Intent intent = new Intent(this, CalendarActivity.class);
            startActivity(intent);
            actionComplete = true;
        } else if (id == R.id.menu_index) {
            Intent intent = new Intent(this, IndexActivity.class);
            startActivity(intent);
            actionComplete = true;
        } else if (id == R.id.menu_download) {
            if (ConnectionService.isNetworkConnectedBoolean(getApplicationContext())) {
                Intent intent = new Intent(this, DocumentActivity.class);
                startActivity(intent);
                actionComplete = true;
            }else {
                Toast.makeText(MainActivity.this,getApplicationContext().getString(R.string.connection_required_messag),Toast.LENGTH_SHORT).show();
                actionComplete = false;
            }
        } else if (id == R.id.menu_faculty) {
            Intent intent = new Intent(this, FacultyActivity.class);
            startActivity(intent);
            actionComplete = true;
        } else if (id == R.id.menu_info) {
            if (ConnectionService.isNetworkConnectedBoolean(getApplicationContext())) {
                Intent intent = new Intent(this, PersonalInfoActivity.class);
                startActivity(intent);
                actionComplete = true;
            }else {
                Toast.makeText(MainActivity.this,getApplicationContext().getString(R.string.connection_required_messag),Toast.LENGTH_SHORT).show();
                actionComplete = false;
            }
        } else if (id == R.id.menu_email) {
            if (ConnectionService.isNetworkConnectedBoolean(getApplicationContext())) {
                Intent intent = new Intent(this, InboxActivity.class);
                startActivity(intent);
                actionComplete = true;
            } else {
                Toast.makeText(MainActivity.this,getApplicationContext().getString(R.string.connection_required_messag),Toast.LENGTH_SHORT).show();
                actionComplete = false;
            }
        }

        if(actionComplete) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

            drawer.closeDrawer(GravityCompat.START);
        }
        return actionComplete;
    }

//    private void updateTodayActivityCount(List<Event> events) {
//        if (events != null) {
//            int totalEventCount = events.size();
//            int nonFinishEventCount = 0;
//            for (Event event : events) {
//                if (eventNotFinish(event)) {
//                    nonFinishEventCount++;
//                }
//            }
//            String count = String.valueOf(nonFinishEventCount).concat("/").concat(String.valueOf(totalEventCount));
//            todayActivityCount.setText(count);
//        } else {
//
//            todayActivityCount.setVisibility(View.INVISIBLE);
//
//        }
//    }

    private int getHour(String date) {
        return Integer.parseInt(date.split("\\.")[0]);
    }

    private int getMinute(String date) {
        return Integer.parseInt(date.split("\\.")[1]);
    }

    private void updateTodayActivityList() {
        this.eventList = EventService.getEventsByDay(LocalDate.now(), LocalDate.now().getDayOfWeek().name(), this);
        userName.setText(person.getName());
        Log.d("Person Name:", person.toString());
        userAisId.setText(person.getAisId());

        currentDayActivities = findViewById(R.id.current_day_list_view);
        if (!eventList.isEmpty()) {
            EventAdapter eventAdapter = new EventAdapter(this, eventList, CalendarDay.today());
            currentDayActivities.setAdapter(eventAdapter);

            currentDayActivities.setVisibility(eventList.size() > 0 ? View.VISIBLE : View.GONE);
            eventNoDataImage.setVisibility(eventList.size() > 0 ? View.GONE : View.VISIBLE);
        } else {
            currentDayActivities.setVisibility(eventList.isEmpty() ? View.GONE : View.GONE);
            eventNoDataImage.setVisibility(eventList.isEmpty() ? View.VISIBLE : View.VISIBLE);
            currentDayActivities.setEmptyView(eventNoDataImage);

        }
        currentDayActivities.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {

                Log.v("long clicked", "pos: " + pos);
                Event event = eventList.get(pos);
                String date = makeDateString(CalendarDay.today().getYear(), CalendarDay.today().getMonth(), CalendarDay.today().getDay());
                if (hasEventNotification(event.getName(), date, event.getStart())) {
                    cancelAlarm(event, date);
                    removeNotification(event.getName(), date, event.getStart());
                    EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList, CalendarDay.today());
                    currentDayActivities.setAdapter(eventAdapter);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(getApplicationContext().getResources().getString(R.string.set_time_before_notification));
                    int itemPosition = 0;
// add a list
                    int checkedItem = 0; // cow
                    builder.setSingleChoiceItems(times, checkedItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // user checked an item
                            int itemPosition = which;
                        }
                    });
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            addNotification(event.getName(), date, event.getStart());
                            setNotification(event, date, itemPosition);
                            EventAdapter eventAdapter = new EventAdapter(getApplicationContext(), eventList, CalendarDay.today());
                            currentDayActivities.setAdapter(eventAdapter);
                        }
                    });
                    builder.setNegativeButton(getApplicationContext().getText(R.string.dialog_cancel_label), null);

// create an

// create and show the alert dialog
                    AlertDialog dialog = builder.create();
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                                    .setTextColor(getApplicationContext()
                                            .getColor(R.color.secondaryTextColor));
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                                    .setTextColor(getApplicationContext()
                                            .getColor(R.color.colorAccent));
                        }
                    });
                    dialog.show();


                }


                //setNotification(event);
                return true;
            }
        });

    }

    private void setContactData() {
        SettingsDBHelper settingsDBHelper = new SettingsDBHelper(getApplicationContext());
        Setting setting = settingsDBHelper.getSettingByType(SettingType.CONTACT_PERSON);
        FacultyContactPersonDBHelper facultyContactPersonDBHelper
                = new FacultyContactPersonDBHelper(this);
        contactPerson =
                facultyContactPersonDBHelper.getFacultyContactById(setting.getValue());
        TextView contactName = findViewById(R.id.contactName);
        TextView contactRoom = findViewById(R.id.contactRoom);
        if (contactPerson != null) {
            contactName.setText(contactPerson.getName());
            contactRoom.setText(contactPerson.getRoom());
        } else {
            contactName.setText(getApplicationContext().getString(R.string.set_contact_person_label));
        }
    }


    public String makeNoteForEvent(Event event) {
        return event.getTeacher();
    }

    public CalendarInfo getCalendar(Context c) {

        CalendarInfo info;

        String projection[] = {CalendarContract.Calendars._ID, CalendarContract.Calendars.OWNER_ACCOUNT};

        ContentResolver contentResolver = c.getContentResolver();

        Cursor calCursor = contentResolver.query(CalendarContract.Calendars.CONTENT_URI, projection,
                CalendarContract.Calendars.VISIBLE + " = 1 AND " + CalendarContract.Calendars.IS_PRIMARY + "=1",
                null, CalendarContract.Calendars._ID + " ASC");
        if (calCursor.getCount() <= 0) {
            calCursor = contentResolver.query(CalendarContract.Calendars.CONTENT_URI, projection,
                    CalendarContract.Calendars.VISIBLE + " = 1", null,
                    CalendarContract.Calendars._ID + " ASC");

            if (calCursor.moveToFirst()) {
                do {
                    String calOwner;
                    Integer calID;
                    calOwner = calCursor.getString(calCursor.getColumnIndex(projection[1]));
                    calID = calCursor.getInt(calCursor.getColumnIndex(projection[0]));
                    if (calOwner.contains("@gmail.com")) {
                        return new CalendarInfo(calID, calOwner);
                    }
                } while (calCursor.moveToNext());
            }
            calCursor = contentResolver.query(CalendarContract.Calendars.CONTENT_URI, projection,
                    CalendarContract.Calendars.VISIBLE + " = 1", null,
                    CalendarContract.Calendars._ID + " ASC");
            if (calCursor.moveToFirst()) {
                return new CalendarInfo(calCursor.getInt(calCursor.getColumnIndex(projection[0])),
                        calCursor.getString(calCursor.getColumnIndex(projection[1])));
            }
            return new CalendarInfo(1, "");
        }

        return new CalendarInfo(calCursor.getInt(calCursor.getColumnIndex(projection[0])),
                calCursor.getString(calCursor.getColumnIndex(projection[1])));
    }

    private void setNotification(Event event, String date, int position) {
        String[] notificationDate = date.split("\\.");
        LocalDate startDate = LocalDate.of(Integer.parseInt(notificationDate[0]),
                Integer.parseInt(notificationDate[1]),
                Integer.parseInt(notificationDate[2]));
        Integer statHour = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[0]);
        Integer statMinute = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[1]);
        LocalDateTime start = LocalDateTime.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth(), statHour, statMinute);
        switch (position) {
            case 0:
                start = start.minusMinutes(15);
                break;
            case 1:
                start = start.minusMinutes(30);
                break;
            case 2:
                start = start.minusHours(1);
                break;
            case 3:
                start = start.minusHours(2);
                break;
            case 4:
                start = start.minusDays(1);
                break;
            default:
                break;

        }
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        Integer id = getNotificationId(event.getName(), date, event.getStart());
        intent.putExtra("eventName", event.getName());
        intent.putExtra("eventDesc", createNotificationDescription(event));
        intent.putExtra("notificationID", id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, 0);


        alarmManager.setExact(AlarmManager.RTC_WAKEUP, start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), pendingIntent);
        Toast.makeText(MainActivity.this, getApplicationContext().getText(R.string.notification_set_seccesfull), Toast.LENGTH_SHORT).show();


    }

    private void cancelAlarm(Event event, String date) {
        Integer id = getNotificationId(event.getName(), date, event.getStart());
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, 0);

        alarmManager.cancel(pendingIntent);
    }

    private String createNotificationDescription(Event event) {
        return event.getRoom();
    }

    private boolean hasEventNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if (notifications != null) {
            if (notifications.isEmpty()) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private String makeDateString(Integer year, Integer month, Integer day) {
        StringBuilder builder = new StringBuilder();
        builder.append(year).append(".");
        builder.append(month).append(".");
        builder.append(day).append(".");
        return builder.toString();
    }

    private void removeNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if (notifications != null) {
            for (EventNotification notification : notifications) {
                dbHelper.deleteNotification(notification);
            }
        }
    }

    private void addNotification(String name, String date, String start) {
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        EventNotification notification = new EventNotification();
        notification.setEventName(name);
        notification.setDate(date);
        notification.setStart(start);
        dbHelper.insertNotification(notification);
    }

    private Integer getNotificationId(String name, String date, String start) {
        Integer id = 1;
        EventNotificationDBHelper dbHelper = new EventNotificationDBHelper(getApplicationContext());
        List<EventNotification> notifications = dbHelper.getAllEventNotificationsByNameAndDateAndStart(name, date, start);
        if (notifications != null) {
            for (EventNotification notification : notifications) {
                id = Integer.parseInt(notification.getId());
            }
        }
        return id;
    }

    private void exportEvent(Event event, CalendarInfo calendarInfo) {
        if (Boolean.parseBoolean(event.getOrigin())) {
            LocalDate startDate = EventService.getDateFromEvent(event.getValidFrom());
            LocalDate endDate = EventService.getDateFromEvent(event.getValidTo());
            for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
                if (DayOfWeek.valueOf(event.getDay()).getNoInWeek().compareTo(date.getDayOfWeek().getValue()) == 0) {

                    Integer statHour = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[0]);
                    Integer statMinute = Integer.parseInt(event.getStart().replace("\n", "").split("\\.")[1]);
                    Integer endHour = Integer.parseInt(event.getEnd().replace("\n", "").split("\\.")[0]);
                    Integer endMinute = Integer.parseInt(event.getEnd().replace("\n", "").split("\\.")[1]);
                    LocalDateTime start = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), statHour, statMinute);
                    LocalDateTime end = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), endHour, endMinute);
                    String[] proj =
                            new String[]{
                                    CalendarContract.Instances.BEGIN,
                                    CalendarContract.Instances.END,
                                    CalendarContract.Instances.TITLE};
                    String title = event.getName();
                    Boolean isDuplicated = false;
                    Cursor cursor =
                            CalendarContract.Instances.query(getContentResolver(), proj,
                                    start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(),
                                    end.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        String eventTitle;
                        if (cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.TITLE)).contains(title)) {
                            isDuplicated = true;
                            continue;
                        }
                        while (cursor.moveToNext()) ;
                        // deal with conflict
                    }

                    if (!isDuplicated) {
                        ContentResolver cr = this.getContentResolver();
                        ContentValues cv = new ContentValues();
                        cv.put(CalendarContract.Events.TITLE, event.getName());
                        cv.put(CalendarContract.Events.DESCRIPTION, makeNoteForEvent(event));
                        cv.put(CalendarContract.Events.EVENT_LOCATION, event.getRoom());
                        cv.put(CalendarContract.Events.DTSTART, start.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                        cv.put(CalendarContract.Events.DTEND, end.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                        cv.put(CalendarContract.Events.CALENDAR_ID, calendarInfo.getId());
                        cv.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
                        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, cv);
                    }
                }
            }

        }
    }

}
