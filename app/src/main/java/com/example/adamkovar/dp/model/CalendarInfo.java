package com.example.adamkovar.dp.model;

public class CalendarInfo {

    Integer id;
    String owner;

    public CalendarInfo(Integer id, String owner) {
        this.id = id;
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
