package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adamkovar.dp.adapter.FacultyContactPersonAdapter;
import com.example.adamkovar.dp.dbHelper.FacultyContactDBHelper;
import com.example.adamkovar.dp.dbHelper.FacultyContactPersonDBHelper;
import com.example.adamkovar.dp.dbHelper.SettingsDBHelper;
import com.example.adamkovar.dp.model.Faculty.FacultyContact;
import com.example.adamkovar.dp.model.Faculty.FacultyContactPerson;
import com.example.adamkovar.dp.model.Faculty.FacultyType;
import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;

import java.util.List;

public class FacultyDetailActivity extends AppCompatActivity {

    private static final String CONTACT_PERSON = "CONTACT_PERSON";

    private static final String VICE_DEAN = "VICE_DEAN";


    private boolean contactSelested;
    FacultyType type;
    List<FacultyContactPerson> contactPersonList;
    ListView contactPersonListVIew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_detail);
        String type = (String) getIntent().getSerializableExtra("faculty");
        this.type = FacultyType.findById(Integer.parseInt(type));
        this.setTitle(this.type.getNameSK());
        TextView facultyAddress = findViewById(R.id.faculty_detail_address_value);
        TextView facultyPhone = findViewById(R.id.faculty_detail_phone_value);
        TextView facultyFax = findViewById(R.id.faculty_detail_fax_value);
        TextView facultyMail = findViewById(R.id.faculty_detail_mail_value);
        TextView facultyNote = findViewById(R.id.faculty_detail_note_value);
        TextView contactLabel = findViewById(R.id.faculty_detail_contacts_label);
        Resources res = getApplicationContext().getResources();
        String contactType =  res.getString(
                res.getIdentifier("contact_type_".concat(String.valueOf(contactSelested)),
                        "string", getApplicationContext().getPackageName()));
        contactLabel.setText(contactType);
        ImageButton switchContactBtn = findViewById(R.id.switch_faculty_contacts_btn);

        contactPersonListVIew = (ListView) findViewById(R.id.faculty_detail_contacts_list);
        contactPersonListVIew.setFastScrollEnabled(true);


        FacultyContactDBHelper contactDBHelper = new FacultyContactDBHelper(this);
        FacultyContactPersonDBHelper personDBHelper = new FacultyContactPersonDBHelper(this);

        FacultyContact contact = contactDBHelper.getFacultyContactByFaculty(this.type.name());
        contactPersonList = personDBHelper.getAllFacultyContactPersonListByFacultyAndPersonType(this.type.name(), CONTACT_PERSON);
        contactSelested = true;
        //contactPersonList.add(new FacultyContactPerson(null,null,null,null,null,null,null,null,null));

        if(!contact.getAddress().equals("null")){
            facultyAddress.setText(contact.getAddress());
        }
        if(!contact.getPhone().equals("null")){
            facultyPhone.setText(contact.getPhone());
        }
        if(!contact.getFax().equals("null")){
            facultyFax.setText(contact.getFax());
        }
        if(!contact.getMail().equals("null")){
            facultyMail.setText(contact.getMail());
        }
        if(!contact.getNote().equals("null")){
            facultyNote.setText(contact.getNote());
        }






        FacultyContactPersonAdapter contactPersonAdapter = new FacultyContactPersonAdapter(this, contactPersonList);
        contactPersonListVIew.setAdapter(contactPersonAdapter);

        switchContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactSelested = !contactSelested;
                if (contactSelested) {
                    if(loadContactByType(CONTACT_PERSON)) {

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.contact_no_contact_data, Toast.LENGTH_SHORT).show();
                        contactSelested = !contactSelested;
                    }
                } else {
                    if(loadContactByType(VICE_DEAN)){

                    } else {
                        Toast.makeText(getApplicationContext(),  R.string.contact_no_vice_dean_data, Toast.LENGTH_SHORT).show();
                        contactSelested = !contactSelested;
                    }
                }
                TextView contactLabel = findViewById(R.id.faculty_detail_contacts_label);
                Resources res = getApplicationContext().getResources();
                String contactType =  res.getString(
                        res.getIdentifier("contact_type_".concat(String.valueOf(contactSelested)),
                                "string", getApplicationContext().getPackageName()));
                contactLabel.setText(contactType);

            }
        });

        contactPersonListVIew.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub

                SettingsDBHelper settingsDBHelper = new SettingsDBHelper(getApplicationContext());
                Setting setting = settingsDBHelper.getSettingByType(SettingType.CONTACT_PERSON);
                setting.setValue(contactPersonList.get(pos).getAisId());
                settingsDBHelper.updateSetting(setting);
                ((BaseAdapter) contactPersonListVIew.getAdapter()).notifyDataSetChanged();
                return true;
            }
        });
    }

    public boolean loadContactByType(String contactType) {
        FacultyContactPersonDBHelper personDBHelper = new FacultyContactPersonDBHelper(this);
        contactPersonList = personDBHelper.getAllFacultyContactPersonListByFacultyAndPersonType(this.type.name(), contactType);
        if (contactPersonList != null) {
            FacultyContactPersonAdapter contactPersonAdapter = new FacultyContactPersonAdapter(this, contactPersonList);
            contactPersonListVIew.setAdapter(contactPersonAdapter);
            contactPersonAdapter.notifyDataSetChanged();
            return true;
        }
        return false;
    }
}
