package com.example.adamkovar.dp.model.Faculty;

public class FacultyContact {

    private String id;
    private String faculty;
    private String name;
    private String address;
    private String phone;
    private String fax;
    private String mail;
    private String note;

    public FacultyContact(String id, String faculty, String name, String address, String phone,
                          String fax, String mail, String note) {
        this.id = id;
        this.faculty = faculty;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
        this.mail = mail;
        this.note = note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
