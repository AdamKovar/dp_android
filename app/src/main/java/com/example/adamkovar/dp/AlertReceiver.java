package com.example.adamkovar.dp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

public class AlertReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String eventName = intent.getStringExtra("eventName");
        String eventDesc = intent.getStringExtra("eventDesc");
        Integer id = intent.getIntExtra("notificationID",1);
        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder nb = notificationHelper.getChannelNotification(eventName, eventDesc);
        notificationHelper.getManager().notify(id, nb.build());
    }
}
