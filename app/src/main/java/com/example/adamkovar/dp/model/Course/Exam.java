package com.example.adamkovar.dp.model.Course;

import java.io.Serializable;

public class Exam implements Serializable {

    private String id;
    private String name;
    private String type;
    private String courseNo;
    private String semesterType;
    private String period;
    private String faculty;
    private String userId;
    private Double maxPoints;
    private Double minPoints;
    private Double earnsPoints;

    public Exam(){}

    public Exam(String id, String name, String type, String courseNo, String semesterType,
                String period, String faculty, String userId, Double maxPoints, Double minPoints,
                Double earnsPoints) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.courseNo = courseNo;
        this.semesterType = semesterType;
        this.period = period;
        this.faculty = faculty;
        this.userId = userId;
        this.maxPoints = maxPoints;
        this.minPoints = minPoints;
        this.earnsPoints = earnsPoints;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }

    public String getSemesterType() {
        return semesterType;
    }

    public void setSemesterType(String semesterType) {
        this.semesterType = semesterType;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Double maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Double getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Double minPoints) {
        this.minPoints = minPoints;
    }

    public Double getEarnsPoints() {
        return earnsPoints;
    }

    public void setEarnsPoints(Double earnsPoints) {
        this.earnsPoints = earnsPoints;
    }
}
