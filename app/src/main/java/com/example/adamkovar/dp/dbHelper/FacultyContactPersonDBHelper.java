package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adamkovar.dp.model.Faculty.FacultyContactPerson;

import java.util.ArrayList;
import java.util.List;

public class FacultyContactPersonDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String FACULTY_CONTACT_PERSON_TABLE_NAME = "facultyContactPerson";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_ID = "id";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_FACULTY = "faculty";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_TYPE = "personType";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_AIS_ID = "aisId";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_NAME = "name";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_ROOM = "room";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_PHONE = "phone";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_MAIL = "mail";
    public static final String FACULTY_CONTACT_PERSON_COLUMN_NOTE = "note";

    public FacultyContactPersonDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS facultyContactPerson");
        onCreate(db);
    }

    public boolean insertFacultyContactPerson(FacultyContactPerson contactPerson) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformFacultyContactPersonToDb(contactPerson);
        db.insert(FACULTY_CONTACT_PERSON_TABLE_NAME, null, contentValues);
        return true;
    }

    public List<FacultyContactPerson> getAllFacultyContactPersonListByFaculty(String faculty) {
        List<FacultyContactPerson> facultyContactPersonList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContactPerson where faculty = '" + faculty + "'", null);
        res.moveToFirst();
        if (res.getCount() <= 0) {
            return null;
        }
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ID)) != null) {
                FacultyContactPerson contactPerson = getFacultyContactPersonFromDB(res);
                facultyContactPersonList.add(contactPerson);
            }
            res.moveToNext();
        }
        return facultyContactPersonList;
    }

    public List<FacultyContactPerson> getAllFacultyContactPersonListByFacultyAndPersonType(String faculty, String personType) {
        List<FacultyContactPerson> facultyContactPersonList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContactPerson where faculty = '"
                + faculty + "' and  personType = '"+ personType +"'", null);
        res.moveToFirst();
        if (res.getCount() <= 0) {
            return null;
        }
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ID)) != null) {
                FacultyContactPerson contactPerson = getFacultyContactPersonFromDB(res);
                facultyContactPersonList.add(contactPerson);
            }
            res.moveToNext();
        }
        return facultyContactPersonList;
    }

    public FacultyContactPerson getFacultyContactById(String aisId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContactPerson where aisId ='"
                + aisId + "'", null);
        res.moveToFirst();
        if (res.getCount() <= 0) {
            return null;
        }
        if (res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ID)) != null) {
            return getFacultyContactPersonFromDB(res);
        }
        return null;
    }



    public FacultyContactPerson getFacultyContactPersonListByFacultyAndAisId(String faculty, String aisId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContactPerson where faculty = '" +
                faculty + "' and aisId ='" + aisId + "'", null);
        res.moveToFirst();
        if (res.getCount() <= 0) {
            return null;
        }
        if (res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ID)) != null) {
            return getFacultyContactPersonFromDB(res);
        }
        return null;
    }


    public boolean updateFacultyContactPerson(FacultyContactPerson facultyContactPerson) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformFacultyContactPersonToDb(facultyContactPerson);
        db.update(FACULTY_CONTACT_PERSON_TABLE_NAME, contentValues, "id = ? ",
                new String[]{facultyContactPerson.getId()});
        return true;
    }

    public List<FacultyContactPerson> getAllFacultyContactPersonList() {
        List<FacultyContactPerson> facultyContactPersonList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from facultyContactPerson", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ID)) != null) {
                FacultyContactPerson contactPerson = getFacultyContactPersonFromDB(res);
                facultyContactPersonList.add(contactPerson);
            }
            res.moveToNext();
        }
        return facultyContactPersonList;
    }

    private ContentValues transformFacultyContactPersonToDb(FacultyContactPerson contactPerson) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_FACULTY, contactPerson.getFaculty());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_TYPE, contactPerson.getPersonType());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_AIS_ID, contactPerson.getAisId());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_NAME, contactPerson.getName());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_ROOM, contactPerson.getRoom());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_PHONE, contactPerson.getPhone());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_MAIL, contactPerson.getMail());
        contentValues.put(FACULTY_CONTACT_PERSON_COLUMN_NOTE, contactPerson.getNote());
        return contentValues;
    }


    private FacultyContactPerson getFacultyContactPersonFromDB(Cursor res) {
        FacultyContactPerson contactPerson = new FacultyContactPerson(res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ID)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_FACULTY)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_TYPE)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_AIS_ID)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_NAME)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_ROOM)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_PHONE)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_MAIL)),
                res.getString(res.getColumnIndex(FACULTY_CONTACT_PERSON_COLUMN_NOTE))
        );
        return contactPerson;
    }
}
