package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.adamkovar.dp.model.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String PERSON_TABLE_NAME = "person";
    public static final String PERSON_COLUMN_ID = "id";
    public static final String PERSON_COLUMN_NAME = "name";
    public static final String PERSON_COLUMN_AISID = "aisId";
    public static final String PERSON_COLUMN_LOGIN = "login";
    public static final String PERSON_COLUMN_TITLE = "title";
    public static final String PERSON_COLUMN_DATEOFBIRTH = "dateOfBirth";
    public static final String PERSON_COLUMN_GENDER = "gender";
    public static final String PERSON_COLUMN_MAIL = "mail";

    public PersonDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS person");
        onCreate(db);
    }

    public boolean insertPerson(Person person) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PERSON_COLUMN_NAME, person.getName());
        contentValues.put(PERSON_COLUMN_TITLE, person.getTitle());
        contentValues.put(PERSON_COLUMN_AISID, person.getAisId());
        contentValues.put(PERSON_COLUMN_LOGIN, person.getLogin());
        contentValues.put(PERSON_COLUMN_DATEOFBIRTH, person.getDateOfBirth());
        contentValues.put(PERSON_COLUMN_GENDER, person.getGender());
        contentValues.put(PERSON_COLUMN_MAIL, person.getMail());
        db.insert(PERSON_TABLE_NAME, null, contentValues);
        return true;
    }

    public Person getPersonByAisId(String aisId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from person where aisId= '" + aisId + "' ", null);
        res.moveToFirst();
        if(res.getCount() <= 0) {
            return null;
        }
        Person person = new Person(res.getString(res.getColumnIndex(PERSON_COLUMN_ID)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_NAME)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_AISID)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_LOGIN)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_TITLE)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_DATEOFBIRTH)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_GENDER)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_MAIL))
        );
        return person;
    }

    public Person getPersonByLogin(String login) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from person where login= '" + login + "' ", null);
        res.moveToFirst();
        if(res.getCount() <= 0) {
            return null;
        }

        Person person = new Person(res.getString(res.getColumnIndex(PERSON_COLUMN_ID)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_NAME)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_AISID)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_LOGIN)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_TITLE)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_DATEOFBIRTH)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_GENDER)),
                res.getString(res.getColumnIndex(PERSON_COLUMN_MAIL))
        );
        return person;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, PERSON_TABLE_NAME);
        return numRows;
    }

    public boolean updatePerson(Person person) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PERSON_COLUMN_NAME, person.getName());
        contentValues.put(PERSON_COLUMN_TITLE, person.getTitle());
        contentValues.put(PERSON_COLUMN_AISID, person.getAisId());
        contentValues.put(PERSON_COLUMN_LOGIN, person.getLogin());
        contentValues.put(PERSON_COLUMN_DATEOFBIRTH, person.getDateOfBirth());
        contentValues.put(PERSON_COLUMN_GENDER, person.getGender());
        contentValues.put(PERSON_COLUMN_MAIL, person.getMail());
        db.update(PERSON_TABLE_NAME, contentValues, "id = ? ", new String[]{person.getId()});
        return true;
    }

    public Integer deletePerson(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(PERSON_TABLE_NAME,
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public List<Person> getAllPersons() {

        List<Person> personList = new ArrayList<Person>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from person", null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(PERSON_COLUMN_ID)) != null) {
                Person person = new Person(res.getString(res.getColumnIndex(PERSON_COLUMN_ID)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_NAME)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_AISID)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_LOGIN)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_TITLE)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_DATEOFBIRTH)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_GENDER)),
                        res.getString(res.getColumnIndex(PERSON_COLUMN_MAIL)));
                personList.add(person);
            }
            res.moveToNext();
        }
        return personList;
    }
}
