package com.example.adamkovar.dp.model.Faculty;

public class FacultyOpenHour {

    private String id;
    private String faculty;
    private String dayOfWeek;
    private String from;
    private String to;
    private String note;

    public FacultyOpenHour(String id, String faculty, String dayOfWeek, String from,
                           String to, String note) {
        this.id = id;
        this.faculty = faculty;
        this.dayOfWeek = dayOfWeek;
        this.from = from;
        this.to = to;
        this.note = note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
