package com.example.adamkovar.dp.model.Event;

import java.io.Serializable;
import java.util.Calendar;

public class Event implements Serializable {

    private String id;
    private String day;
    private String name;
    private String courseNo;
    private String start;
    private String end;
    private String room;
    private String teacher;
    private String eventType;
    private String login;
    private String validFrom;
    private String validTo;
    private String origin;
    private String eventPeriodicity;

    public Event() {
    }

    public Event(String id, String day, String name, String courseNo, String start, String end,
                 String room, String teacher, String eventType, String validFrom,
                 String validTo, String origin, String eventPeriodicity, String login) {
        this.id = id;
        this.day = day;
        this.name = name;
        this.courseNo = courseNo;
        this.start = start;
        this.end = end;
        this.room = room;
        this.teacher = teacher;
        this.eventType = eventType;
        this.login = login;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.origin = origin;
        this.eventPeriodicity = eventPeriodicity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getEventPeriodicity() {
        return eventPeriodicity;
    }

    public void setEventPeriodicity(String eventPeriodicity) {
        this.eventPeriodicity = eventPeriodicity;
    }
}
