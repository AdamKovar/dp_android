package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.adamkovar.dp.R;
import com.example.adamkovar.dp.model.PersonalInfo;

import java.util.ArrayList;
import java.util.List;

public class PersonalInfoAdapter extends ArrayAdapter<PersonalInfo> {

    Context context;

    private List<PersonalInfo> personalInfoList = new ArrayList<>();

    public PersonalInfoAdapter(Context c, List<PersonalInfo> personalInfoList) {
        super(c, R.layout.personal_info_row);
        this.context = c;
        this.personalInfoList = personalInfoList;
    }

    public PersonalInfoAdapter(Context c) {
        super(c, R.layout.personal_info_row);
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.personal_info_row, parent, false);

        TextView infoLabel = view.findViewById(R.id.info_label);
        TextView infoValue = view.findViewById(R.id.info_value);

        infoLabel.setText(personalInfoList.get(position).getLabel());
        infoValue.setText(personalInfoList.get(position).getValue()
                .replace(" - ","\n")
                .replace(", ","\n"));
        return view;
    }


    @Override
    public int getCount() {
        return personalInfoList.size();
    }

    @NonNull
    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public List<PersonalInfo> getPersonalInfoList() {
        return personalInfoList;
    }

    public void setPersonalInfoList(List<PersonalInfo> personalInfoList) {
        this.personalInfoList = personalInfoList;
    }
}

