package com.example.adamkovar.dp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.adamkovar.dp.R;


public class FacultyTypeAdapter extends BaseAdapter {

    Context context;

    private Integer[ ] facultyImages = {
            R.drawable.stu,
            R.drawable.stu_svf,
            R.drawable.stu_sjf,
            R.drawable.stu_fei,
            R.drawable.stu_fchpt,
            R.drawable.stu_fa,
            R.drawable.stu_mtf,
            R.drawable.stu_fiit
    };

    private LayoutInflater thisInflater;

    public FacultyTypeAdapter() {
        super();
    }

    public FacultyTypeAdapter(Context context) {
        this.thisInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return facultyImages.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView  == null){
            convertView = thisInflater.inflate(R.layout.faculty_item, parent, false);

            ImageView facultyLogo = (ImageView) convertView.findViewById(R.id.faculty_item_image);

            facultyLogo.setImageResource(facultyImages[position]);

        }
        return convertView;
    }


}
