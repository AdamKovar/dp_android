package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adamkovar.dp.model.Course.Exam;
import com.example.adamkovar.dp.model.Event.Event;
import com.example.adamkovar.dp.model.EventNotification;

import java.util.ArrayList;
import java.util.List;

public class EventNotificationDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String NOTIFICATION_TABLE_NAME = "notification";
    public static final String NOTIFICATION_COLUMN_ID = "id";
    public static final String NOTIFICATION_COLUMN_NOTIFICATION_ID = "notificationId";
    public static final String NOTIFICATION_COLUMN_NAME = "eventName";
    public static final String NOTIFICATION_COLUMN_DATE = "date";
    public static final String NOTIFICATION_COLUMN_START = "start";


    public boolean insertNotification(EventNotification notification) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = transformNotificationToDb(notification);
        db.insert(NOTIFICATION_TABLE_NAME, null, contentValues);
        return true;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS notification");
        onCreate(db);
    }

    public EventNotificationDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public boolean deleteNotification(EventNotification notification) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(NOTIFICATION_TABLE_NAME, "id = ? ",
                new String[]{notification.getId()});
        return true;
    }


    public List<EventNotification> getAllNotifications() {
        List<EventNotification> notificaitons = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notification", null);
        if (res.getCount() <= 0) {
            return null;
        }
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_ID)) != null) {
                EventNotification notification = getEventNotificationFromDB(res);
                notificaitons.add(notification);
            }
            res.moveToNext();
        }
        return notificaitons;
    }

    public List<EventNotification> getAllEventNotificationsByNameAndDateAndStart(String name, String date, String start) {
        List<EventNotification> notifications = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notification where eventName= '" + name + "' and date = '"+date+"' and start = '"+start+"'", null);
        if (res.getCount() <= 0) {
            return null;
        }
        res.moveToFirst();
        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_ID)) != null) {
                EventNotification notification = getEventNotificationFromDB(res);
                notifications.add(notification);
            }
            res.moveToNext();
        }
        return notifications;
    }


    private EventNotification getEventNotificationFromDB(Cursor res) {
        EventNotification notification = new EventNotification(res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_ID)),
                res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_NOTIFICATION_ID)),
                res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_NAME)),
                res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_DATE)),
                res.getString(res.getColumnIndex(NOTIFICATION_COLUMN_START)));
        return notification;
    }

    private ContentValues transformNotificationToDb(EventNotification notification) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOTIFICATION_COLUMN_NOTIFICATION_ID, notification.getNotificationID());
        contentValues.put(NOTIFICATION_COLUMN_NAME, notification.getEventName());
        contentValues.put(NOTIFICATION_COLUMN_DATE, notification.getDate());
        contentValues.put(NOTIFICATION_COLUMN_START, notification.getStart());
        return contentValues;
    }
}
