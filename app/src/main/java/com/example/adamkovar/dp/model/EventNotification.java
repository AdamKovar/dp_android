package com.example.adamkovar.dp.model;

public class EventNotification {

    private String id;

    private String notificationID;

    private String eventName;

    private String date;

    private String start;

    public EventNotification(String id, String notificationID, String eventName, String date, String start) {
        this.id = id;
        this.notificationID = notificationID;
        this.eventName = eventName;
        this.date = date;
        this.start = start;
    }

    public EventNotification() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }
}
