package com.example.adamkovar.dp.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.adamkovar.dp.model.Setting;
import com.example.adamkovar.dp.model.SettingType;

import java.util.ArrayList;
import java.util.List;

public class SettingsDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DP.db";
    public static final String SETTINGS_TABLE_NAME = "settings";
    public static final String SETTINGS_COLUMN_ID = "id";
    public static final String SETTINGS_COLUMN_NAME = "name";
    public static final String SETTINGS_COLUMN_VALUE= "value";

    public SettingsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table person " +
                        "(id integer primary key, name text, aisId text, login text, title text," +
                        " dateOfBirth text, gender text, mail text )"
        );
        db.execSQL(
                "create table course " +
                        "(id integer primary key, name text, ref text, code text," +
                        " type text, lang text, examType text, examAttempt text, examMark text," +
                        " credit text, semesterType text, period text, faculty text, userId text," +
                        " minCredit int, maxCredit int, minExam int, maxExam int )"
        );

        db.execSQL( "create table exam " +
                "(id integer primary key, name text, type text, courseNo text, semesterType text," +
                " period text, faculty text, userId text," +
                " maxPoints real, minPoints real, earnsPoints real)"
        );

        db.execSQL("create table facultyContact " +
                "(id integer primary key, faculty text, name text, address text, phone text," +
                " fax text, mail text, note text )"
        );

        db.execSQL("create table facultyContactPerson " +
                "(id integer primary key, faculty text, personType text, aisId text, name text," +
                " room text, phone text, mail text, note text )"
        );

        db.execSQL("create table facultyOpenHour " +
                "(id integer primary key, faculty text, day text, openFrom text, openTo text," +
                " note text)"
        );

        db.execSQL("create table event " +
                "(id integer primary key, day text, name text,  courseNo,  startDate text, endDate text,\n" +
                "                  room text,  teacher text,  eventType text,  login text,  validFrom text,\n" +
                "                  validTo text,  origin text,  eventPeriodicity text)"
        );

        db.execSQL("create table settings " +
                "(id integer primary key, name text, value text)"
        );

        db.execSQL("create table notification " +
                "(id integer primary key, notificationId text, eventName text,  date text,  start text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS settings");
        onCreate(db);
    }

    public boolean insertSetting(Setting setting) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SETTINGS_COLUMN_NAME, setting.getName());
        contentValues.put(SETTINGS_COLUMN_VALUE, setting.getValue());
        db.insert(SETTINGS_TABLE_NAME, null, contentValues);
        return true;
    }

    public Setting getSettingByType(SettingType settingType) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from settings where name= '" + settingType.name() + "' ", null);
        res.moveToFirst();
        if(res.getCount() <= 0) {
            return null;
        }
        Setting setting = new Setting(res.getString(res.getColumnIndex(SETTINGS_COLUMN_ID)),
                res.getString(res.getColumnIndex(SETTINGS_COLUMN_NAME)),
                res.getString(res.getColumnIndex(SETTINGS_COLUMN_VALUE))
        );
        return setting;
    }



    public boolean updateSetting(Setting setting) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SETTINGS_COLUMN_NAME, setting.getName());
        contentValues.put(SETTINGS_COLUMN_VALUE, setting.getValue());
        db.update(SETTINGS_TABLE_NAME, contentValues, "id = ? ", new String[]{setting.getId()});
        return true;
    }

    public Integer deleteSetting(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(SETTINGS_TABLE_NAME,
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public List<Setting> getAllSettings() {

        List<Setting> settings = new ArrayList<Setting>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from person", null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            if (res.getString(res.getColumnIndex(SETTINGS_COLUMN_ID)) != null) {
                Setting setting = new Setting(res.getString(res.getColumnIndex(SETTINGS_COLUMN_ID)),
                        res.getString(res.getColumnIndex(SETTINGS_COLUMN_NAME)),
                        res.getString(res.getColumnIndex(SETTINGS_COLUMN_VALUE)));
                settings.add(setting);
            }
            res.moveToNext();
        }
        return settings;
    }
}
