package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.adamkovar.dp.model.EmailMessage;

public class EmailDetailActivity extends AppCompatActivity {

    private EmailMessage email;
    TextView emailSender, emailDate, emailTime, emailSubject, emailContent, emailAcronym;

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_detail);
        this.email = (EmailMessage) getIntent().getSerializableExtra("EmailMessage");
        emailSender = findViewById(R.id.email_sender);
        emailDate = findViewById(R.id.email_date);
        emailTime = findViewById(R.id.email_time);
        emailSubject = findViewById(R.id.email_subject);
        emailContent = findViewById(R.id.email_content);
        emailAcronym = findViewById(R.id.email_acronym);

        emailSender.setText(email.getFrom());
        emailDate.setText(email.getSentDate());
        emailTime.setText(email.getSentTime());
        emailSubject.setText(email.getSubject());
        emailContent.setText(email.getMessage());
        if(email.getFrom().toCharArray()[0] == 'x') {
            emailAcronym.setText(email.getFrom().substring(1,2).toUpperCase());
        } else {
            emailAcronym.setText(email.getFrom().substring(0,1).toUpperCase());
        }

    }


}
