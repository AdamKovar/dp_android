package com.example.adamkovar.dp.service;

import android.content.Context;
import android.net.ConnectivityManager;

import com.example.adamkovar.dp.model.ConnestionStatus;

import java.net.InetAddress;

public class ConnectionService {

    public static ConnestionStatus isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Boolean connected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        ConnestionStatus status = new ConnestionStatus();
        if (connected) {
            status.setMessage("Connection Successful");
        } else {
            status.setMessage("Connection Failed");
        }
        status.setConnected(connected);
        return status;
    }

    public static boolean isNetworkConnectedBoolean(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Boolean connected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        ConnestionStatus status = new ConnestionStatus();
        if (connected) {
            status.setMessage("Connection Successful");
            return true;
        } else {
            status.setMessage("Connection Failed");
            return false;
        }
    }

    public static ConnestionStatus isServerAvailable(Context context) {
        ConnestionStatus status = new ConnestionStatus();
        if (isNetworkConnected(context).isConnected()) {
            try {
                InetAddress ipAddr = InetAddress.getByName("google.com");
                //You can replace it with your name
                Boolean connected = !ipAddr.equals("");
                if (connected) {
                    status.setMessage("server is available");
                } else {
                    status.setMessage("server is NOT available");
                }
                status.setConnected(connected);
            } catch (Exception e) {
                return new ConnestionStatus(false, "Server connection FAILED");
            }
        } else {
            status.setConnected(Boolean.FALSE);
            status.setMessage("Connection Failed");
        }
        return status;
    }
}
