package com.example.adamkovar.dp.model.Event;

import java.io.Serializable;

public enum EventPeriodicity implements Serializable {

    DAILY(1),
    ONCEPERWEEK(2),
    ONLYONCE(3);


    private int id;

    EventPeriodicity(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
