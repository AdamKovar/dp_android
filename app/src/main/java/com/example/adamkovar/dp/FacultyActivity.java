package com.example.adamkovar.dp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.adamkovar.dp.adapter.FacultyTypeAdapter;
import com.example.adamkovar.dp.dbHelper.FacultyContactPersonDBHelper;
import com.example.adamkovar.dp.dbHelper.FacultyOpenHourDBHelper;
import com.example.adamkovar.dp.dbHelper.PersonDBHelper;
import com.example.adamkovar.dp.model.Faculty.FacultyContactPerson;
import com.example.adamkovar.dp.model.Faculty.FacultyOpenHour;
import com.example.adamkovar.dp.model.Faculty.FacultyType;
import com.example.adamkovar.dp.model.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FacultyActivity extends AppCompatActivity {

    private static final String BASE_URL = "http://147.175.121.89:8085/rest/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty);

        GridView facultyGridView = (GridView) findViewById(R.id.faculty_grid_view);
        FacultyTypeAdapter facultyTypeAdapter = new FacultyTypeAdapter(getApplicationContext());
        facultyGridView.setAdapter(facultyTypeAdapter);
        facultyGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String type = "";
                if (position == 0) {
                    type = FacultyType.FACULTY_STU.getId().toString();
                } else {
                    type = Integer.toString(FacultyType.findById(position * 10).getId());
                }
                PersonDBHelper personDBHelper = new PersonDBHelper(getApplicationContext());
                Person person = personDBHelper.getAllPersons().stream().findFirst().orElse(null);
                changeActivity(getApplicationContext(), type);

                // Toast.makeText(getApplicationContext(),"You click on: "+type, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void laodData(){}

    public static void changeActivity(Context context, String facultyType) {
        Intent intent = new Intent(context, FacultyDetailActivity.class);
        intent.putExtra("faculty", facultyType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


}
